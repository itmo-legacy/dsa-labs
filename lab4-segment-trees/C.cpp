#include <limits>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>
#include <experimental/optional>

using std::experimental::optional;

const std::string taskname = "rmq2";

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    using value_t = std::int64_t;

    struct Min : public Monoid<value_t> {
        static const value_t MEMPTY() { return std::numeric_limits<value_t>::max(); }

        static value_t MAPPEND(value_t const &one, value_t const &two) {
            return std::min(one, two);
        }
    };

    struct Adjustment {
        bool is_addition;
        union {
            value_t adjustment;
            value_t addition;
        } box;

        Adjustment() : Adjustment(true, 0) {}

        Adjustment(bool is_addition, value_t value) : is_addition(is_addition) {
            (is_addition ? box.addition : box.adjustment) = value;
        }

        Adjustment operator+(const Adjustment &other) const {
            if (not other.is_addition) {
                return other;
            } else if (not is_addition) {
                return {false, box.adjustment + other.box.addition};
            } else {
                return {true, box.addition + other.box.addition};
            }
        }

        value_t value() {
            return is_addition ? box.addition : box.adjustment;
        }
    };

    using AdjustmentM = Monoid<Adjustment>;

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        value_t value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        Node(size_t lbound, size_t rbound, Node *lchild, Node *rchild, value_t value)
                : lbound(lbound), rbound(rbound), lchild(lchild), rchild(rchild), value(value) {}

        constexpr bool is_leaf() { return rbound - lbound == 1; }

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
                update();
            } else {
                value = adjusted();
                adjustment = AdjustmentM::MEMPTY();
            }
        }

        void update() {
            value = Min::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        value_t adjusted() { return AdjustmentM::MAPPEND({false, value}, adjustment).value(); }

        friend value_t get_value(Node *node) { return node == nullptr ? Min::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    static value_t
    min(Node *node, size_t left_interval_bound, size_t right_interval_bound) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) {
            return Min::MEMPTY();
        }
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            return node->adjusted();
        }
        node->push();
        return Min::MAPPEND(min(node->lchild, left_interval_bound, right_interval_bound),
                            min(node->rchild, left_interval_bound, right_interval_bound));
    }

    Node *root = nullptr;

    explicit SegmentTree(std::vector<value_t> const &vector) {
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<value_t> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        Min::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    void set(size_t lbound, size_t rbound, value_t value) { change(root, lbound, rbound, {false, value}); }

    void add(size_t lbound, size_t rbound, value_t value) { change(root, lbound, rbound, {true, value}); }

    value_t min(size_t lbound, size_t rbound) { return min(root, lbound, rbound); }
};

using value_t = SegmentTree::value_t;

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<value_t> nums(n);
    for (size_t i = 0; i < n; ++i) {
        in >> nums[i];
    }
    SegmentTree tree(nums);
    std::string operation;
    while (in >> operation) {
        size_t i, j;
        in >> i >> j;
        if (operation == "set") {
            value_t x;
            in >> x;
            tree.set(i - 1, j, x);
        } else if (operation == "add") {
            value_t x;
            in >> x;
            tree.add(i - 1, j, x);
        } else {
            out << tree.min(i - 1, j) << std::endl;
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}

