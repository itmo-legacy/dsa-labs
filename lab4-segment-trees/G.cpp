#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>

const std::string taskname = "sparse";

constexpr size_t SIZE = 400'010;

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        int max;
        int y;
    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() { return Value{std::numeric_limits<int>::min(), 0}; };

        static Value MAPPEND(Value const &one, Value const &two) { return one.max > two.max ? one : two; }
    };

    using Adjustment = int;

    using AdjustmentM = Monoid<Adjustment>;

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
                update();
            } else {
                value = adjusted();
                adjustment = AdjustmentM::MEMPTY();
            }
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() { return {value.max + adjustment, value.y}; }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    SegmentTree() {
        std::vector<Value> vector(SIZE);
        for (std::size_t i = 0; i < SIZE; ++i) {
            vector[i].y = (int) i;
        }
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    void add(size_t lbound, size_t rbound, int value) { change(root, lbound, rbound, {value}); }

    std::pair<int, int> get() {
        root->push();
        return {root->value.max, root->value.y};
    }
};

struct Event {
    int x;
    enum {
        IN = 0, OUT = 1
    } type;
    size_t bottom, top;

    bool operator<(Event const &other) {
        if (x != other.x) { return x < other.x; }
        else { return type < other.type; }
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<Event> scanline;
    for (std::size_t i = 0; i < n; ++i) {
        int x1, y1, x2, y2;
        in >> x1 >> y1 >> x2 >> y2;
        size_t adj_y1 = y1 + SIZE / 2, adj_y2 = y2 + SIZE / 2;
        scanline.push_back({x1, Event::IN, adj_y1, adj_y2});
        scanline.push_back({x2, Event::OUT, adj_y1, adj_y2});
    }
    std::sort(scanline.begin(), scanline.end());
    SegmentTree tree;
    int max = std::numeric_limits<int>::min();
    int x = -1, y = -1;
    for (auto &event: scanline) {
        if (event.type == Event::IN) {
            tree.add(event.bottom, event.top + 1, 1);
            int lmax, ly;
            std::tie(lmax, ly) = tree.get();
            if (lmax > max) { std::tie(max, x, y) = std::tie(lmax, event.x, ly); }
        } else {
            tree.add(event.bottom, event.top + 1, -1);
        }
    }
    out << max << std::endl
        << x << " " << int(y - SIZE / 2) << std::endl;
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
        solve(std::cin, std::cout);
    }
    return 0;
}