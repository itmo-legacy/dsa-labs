#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>

const std::string taskname = "rmq";

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    using Value = std::int32_t;

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() { return Value{std::numeric_limits<Value>::max()}; };

        static Value MAPPEND(Value const &one, Value const &two) { return std::min(one, two); }
    };

    using Adjustment = std::int32_t;

    struct AdjustmentM : public Monoid<Adjustment> {
        static const Adjustment MEMPTY() { return Adjustment{std::numeric_limits<Adjustment>::min()}; }

        static Adjustment MAPPEND(Adjustment const &one, Adjustment const &two) { return std::max(one, two); }
    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        Value get(size_t l, size_t r) {
            if (r <= lbound or rbound <= l) { return ValueM::MEMPTY(); }
            push();
            if (l <= lbound and rbound <= r) { return adjusted(); }
            return ValueM::MAPPEND(lchild->get(l, r), rchild->get(l, r));
        }

        void fill(std::vector<Value> &vector) {
            if (is_leaf()) {
                vector[lbound] = adjusted();
            } else {
                push();
                lchild->fill(vector);
                rchild->fill(vector);
            }
        }

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
            }
            value = adjusted();
            adjustment = AdjustmentM::MEMPTY();
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() { return std::max(value, adjustment); }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    explicit SegmentTree(std::vector<Value> const &vector) {
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
    }

    void update(size_t lbound, size_t rbound, Value value) { change(root, lbound, rbound, value); }

    Value get(size_t l, size_t r) {
        return root->get(l, r);
    }

    std::vector<Value> list() {
        std::vector<Value> result(root->length());
        root->fill(result);
        return result;
    }
};

using Value = SegmentTree::Value;

struct Query {
    size_t l, r;
    Value expected;
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    std::vector<Value> vector(n, std::numeric_limits<Value>::min());
    SegmentTree tree(vector);
    std::vector<Query> queries;
    queries.reserve(m);
    for (std::size_t i = 0; i < m; ++i) {
        size_t l, r;
        Value expected;
        in >> l >> r >> expected;
        queries.push_back({l - 1, r, expected});
        tree.update(l - 1, r, expected);
    }

    bool inconsistent = false;
    for (auto &query: queries) {
        if (tree.get(query.l, query.r) != query.expected) { inconsistent = true; }
    }
    if (inconsistent) {
        out << "inconsistent" << std::endl;
    } else {
        out << "consistent" << std::endl;
        for (auto &el: tree.list()) {
            out << el << " ";
        }
        out << std::endl;
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
