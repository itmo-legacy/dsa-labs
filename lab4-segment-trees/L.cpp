#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <experimental/optional>
#include <cmath>
#include <random>

using std::experimental::optional;

const std::string taskname = "parking";

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        size_t leftmost_unset;
        size_t rightmost_unset;

        Value(size_t l, size_t r) : leftmost_unset(l), rightmost_unset(r) {}

        bool operator==(Value const &other) const { return leftmost_unset == other.leftmost_unset; }
    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() {
            return Value{std::numeric_limits<size_t>::max(), std::numeric_limits<size_t>::min()};
        }

        static Value MAPPEND(Value const &one, Value const &two) {
            return {std::min(one.leftmost_unset, two.leftmost_unset),
                    std::max(one.rightmost_unset, two.rightmost_unset)};
        }
    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        Value update(size_t place) {
            if (value.leftmost_unset >= place) {
                return value;
            } else {
                if (is_leaf()) { return ValueM::MEMPTY(); }
                else if (lchild->value.rightmost_unset >= place) { return lchild->update(place); }
                else { return rchild->update(place); }
            }
        }

        void set(size_t place, bool set) {
            if (is_leaf()) {
                if (lbound == place) { value = set ? ValueM::MEMPTY() : Value{lbound, lbound}; }
            } else {
                if (place < middle(lbound, rbound)) { lchild->set(place, set); }
                else { rchild->set(place, set); }
                _update();
            }
        }

        void _update() {
            value = ValueM::MAPPEND(lchild->value, rchild->value);
        }
    };

    Node *root = nullptr;

    explicit SegmentTree(size_t n) {
        std::vector<Value> vector;
        vector.reserve(n);
        for (std::size_t i = 0; i < n; ++i) { vector.emplace_back(i, i); }
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        else if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(lchild->value, rchild->value)};
    }

    size_t update(size_t place) {
        Value possible_result = root->update(place);
        if (possible_result == ValueM::MEMPTY()) {
            possible_result = root->update(0);
        }
        set(possible_result.leftmost_unset, true);
        return possible_result.leftmost_unset;
    }

    void set(size_t place, bool set) { root->set(place, set); }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    SegmentTree tree(n);
    for (std::size_t _ = 0; _ < m; ++_) {
        std::string instruction;
        size_t place;
        in >> instruction >> place;
        --place;
        if (instruction == "enter") {
            out << tree.update(place) + 1 << "\n";
        } else {
            tree.set(place, false);
        }
    }
    out.flush();
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}