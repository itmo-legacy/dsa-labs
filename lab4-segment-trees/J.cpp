#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <experimental/optional>

using std::experimental::optional;

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        int min;
        size_t place;
    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() { return Value{std::numeric_limits<int>::max(), 0}; };

        static Value MAPPEND(Value const &one, Value const &two) { return one.min < two.min ? one : two; }
    };

    using Adjustment = int;

    struct AdjustmentM : public Monoid<Adjustment> {
        static const Adjustment MEMPTY() { return std::numeric_limits<Adjustment>::min(); }

        static Adjustment MAPPEND(Adjustment const &one, Adjustment const &two) { return std::max(one, two); }
    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        Value get(size_t l, size_t r) {
            if (r <= lbound or rbound <= l) { return ValueM::MEMPTY(); }
            push();
            if (l <= lbound and rbound <= r) { return value; }
            else { return {ValueM::MAPPEND(lchild->get(l, r), rchild->get(l, r))}; }
        };

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
                update();
            } else {
                value = adjusted();
                adjustment = AdjustmentM::MEMPTY();
            }
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() { return {std::max(value.min, adjustment), value.place}; }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    explicit SegmentTree(std::size_t n) {
        std::vector<Value> vector;
        vector.reserve(n);
        for (std::size_t i = 0; i < n; ++i) {
            vector.push_back({0, i});
        }
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    void update(size_t lbound, size_t rbound, int value) { change(root, lbound, rbound, {value}); }

    std::pair<int, size_t> get(size_t l, size_t r) {
        auto result = root->get(l, r);
        return {result.min, result.place};
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    SegmentTree tree(n);
    for (std::size_t _ = 0; _ < m; ++_) {
        std::string instruction;
        size_t l, r;
        in >> instruction >> l >> r;
        l -= 1;
        if (instruction == "defend") {
            int height;
            in >> height;
            tree.update(l, r, height);
        } else {
            int min;
            size_t place;
            std::tie(min, place) = tree.get(l, r);
            out << min << " " << place + 1 << "\n";
        }
    }
    out.flush();
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
        solve(std::cin, std::cout);
    }
    return 0;
}