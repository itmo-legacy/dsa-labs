template<typename ValueT, ValueT Identity, typename AppendT>
class Monoid {
private:
    static constexpr ValueT EMPTY = Identity;
    static constexpr AppendT APPEND = AppendT();

    const ValueT value = Identity;
public:
    using monoid = Monoid<ValueT, Identity, AppendT>;

    constexpr Monoid() noexcept = default;

    constexpr Monoid(const ValueT &value) noexcept : value(value) {};

    constexpr ValueT getValue() const noexcept {
        return value;
    }

    friend Monoid
    mappend(const Monoid &one, const Monoid &two) {
        return {APPEND(one.getValue(), two.getValue())};
    }

    template<typename V, V I, typename AT>
    friend Monoid<V, I, AT>
    mempty();
};

template<typename V, V I, typename AT>
Monoid<V, I, AT> mempty() {
    return {Monoid<V, I, AT>::EMPTY};
}
