#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

const std::string taskname = "rsq";


template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

constexpr int find_height(size_t x) {
    return x == 0 ? 0 : 1 + find_height(x >> 1U);
}

struct SegmentTree {
    using value_t = std::int64_t;
    constexpr static value_t MEMPTY = 0;
    constexpr static std::plus<> MAPPEND = std::plus<>();

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        value_t value;

        bool is_leaf() { return rbound - lbound == 1; }

        void set(size_t index, value_t value) {
            if (lbound <= index and index < rbound) {
                if (is_leaf()) {
                    modify(value);
                } else {
                    size_t m = middle(lbound, rbound);
                    (index < m ? lchild : rchild)->set(index, value);
                    update();
                }
            }
        }

        value_t sum(size_t left_interval_bound, size_t right_interval_bound) {
            if (right_interval_bound <= lbound or rbound <= left_interval_bound) { return MEMPTY; }
            if (left_interval_bound <= lbound && rbound <= right_interval_bound) { return value; }
            return MAPPEND(lchild->sum(left_interval_bound, right_interval_bound),
                           rchild->sum(left_interval_bound, right_interval_bound));
        }

    private:
        void modify(value_t value) {
            this->value = value;
        }

        void update() {
            value = MAPPEND(get_value(lchild), get_value(rchild));
            value = MAPPEND(lchild == nullptr ? MEMPTY : lchild->value, rchild == nullptr ? MEMPTY : rchild->value);
        }

        friend constexpr value_t get_value(Node *node) { return node == nullptr ? MEMPTY : node->value; }
    };


    void combine(Node *&node, Node *lchild, Node *rchild) {
        if (lchild == nullptr) { return; }
        size_t rbound = rchild == nullptr ? lchild->rbound : rchild->rbound;
        node = new Node{lchild->lbound, rbound, lchild, rchild, MAPPEND(get_value(lchild), get_value(rchild))};
    }

    Node *root = nullptr;

    explicit SegmentTree(std::vector<value_t> const &vector) {
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<value_t> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
            return;
        }
        size_t m = middle(lbound, rbound);
        Node *lchild = nullptr;
        build(lchild, lbound, m, vector);
        Node *rchild = nullptr;
        build(rchild, m, rbound, vector);
        combine(node, lchild, rchild);
    }

    void set(size_t index, value_t value) {
        root->set(index, value);
    }

    value_t sum(size_t lbound, size_t rbound) {
        return root->sum(lbound, rbound);
    }
};

using value_t = SegmentTree::value_t;
//const value_t SegmentTree::MEMPTY = 0;
//const auto SegmentTree::MAPPEND = std::plus<>();

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<value_t> nums(n);
    for (size_t i = 0; i < n; ++i) {
        in >> nums[i];
    }
    SegmentTree tree(nums);
    std::string operation;
    while (in >> operation) {
        if (operation == "set") {
            size_t i;
            value_t x;
            in >> i >> x;
            tree.set(i - 1, x);
        } else {
            size_t i, j;
            in >> i >> j;
            out << tree.sum(i - 1, j) << std::endl;
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}
