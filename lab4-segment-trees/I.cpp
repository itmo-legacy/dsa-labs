#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <experimental/optional>

using std::experimental::optional;

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        int max;
        int right_height;
    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() { return Value{std::numeric_limits<int>::min(), 0}; };

        static Value MAPPEND(Value const &one, Value const &two) {
            int new_max = std::max(one.max, two.max + one.right_height);
            return {new_max, one.right_height + two.right_height};
        }
    };

    using Adjustment = optional<int>;

    struct AdjustmentM : public Monoid<Adjustment> {
        static Adjustment MAPPEND(Adjustment const &one, Adjustment const &two) { return two ? two : one; }
    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        size_t get(int height) {
            push();
            if (is_leaf()) { return value.max >= height ? lbound : rbound; }
            if (get_value(lchild).max >= height) { return lchild->get(height); }
            else { return rchild->get(height - get_value(lchild).right_height); }
        }

        void push() {
            if (not is_leaf()) {
                createChildren();
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
            }
            value = adjusted();
            adjustment = AdjustmentM::MEMPTY();
        }

        void createChildren() {
            if (not(lchild and rchild)) {
                size_t m = middle(lbound, rbound);
                lchild = new Node{lbound, m, nullptr, nullptr, {0, 0}};
                rchild = new Node{m, rbound, nullptr, nullptr, {0, 0}};
            }
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() {
            if (adjustment == AdjustmentM::MEMPTY()) { return value; }
            int new_right_height = int(adjustment.value() * length());
            int new_min = std::max(new_right_height, 0);
            return {new_min, new_right_height};
        }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    explicit SegmentTree(size_t n) { root = new Node{0, n, nullptr, nullptr, {0, 0}}; }

    void update(size_t lbound, size_t rbound, int height) { change(root, lbound, rbound, {height}); }

    size_t get(int height) { return root->get(height); }

};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    SegmentTree tree(n);
    std::string instruction;
    in >> instruction;
    while (instruction != "E") {
        if (instruction == "I") {
            size_t l, r;
            int change;
            in >> l >> r >> change;
            tree.update(l - 1, r, change);
        } else {
            int height;
            in >> height;
            out << tree.get(height + 1) << std::endl;
        }
        in >> instruction;
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
        solve(std::cin, std::cout);
    }
    return 0;
}