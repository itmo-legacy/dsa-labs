#include <limits>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>
#include <experimental/optional>

using std::experimental::optional;

const std::string taskname = "painter";

constexpr size_t SIZE = 1'000'010;

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        bool left_set = false;
        bool right_set = false;
        size_t set_intervals_count = 0;
        size_t set_count = 0;
        bool is_empty = false;

        Value operator+(const Value &other) const {
            if (is_empty) { return other; }
            if (other.is_empty) { return *this; }
            return {left_set, other.right_set,
                    set_intervals_count + other.set_intervals_count - (right_set and other.left_set),
                    set_count + other.set_count};
        }
    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() {
            Value result;
            result.is_empty = true;
            return result;
        };
    };

    struct Adjustment {
        int set = -1;

        Adjustment operator+(const Adjustment &other) const { return other.set == -1 ? *this : other; }
    };

    using AdjustmentM = Monoid<Adjustment>;

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
                update();
            } else {
                value = adjusted();
                adjustment = AdjustmentM::MEMPTY();
            }
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() {
            if (adjustment.set == 1) {
                return {true, true, 1, length()};
            } else if (adjustment.set == 0) {
                return {false, false, 0, 0};
            } else {
                return value;
            }
        }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    SegmentTree() {
        std::vector<Value> vector(SIZE, {false, false, 0, 0});
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    void set(size_t lbound, size_t rbound, bool value) { change(root, lbound, rbound, {value}); }

    std::pair<size_t, size_t> get() {
        root->push();
        return {root->value.set_intervals_count, root->value.set_count};
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    SegmentTree tree;
    while (n-- > 0) {
        std::string operation;
        size_t start, length;
        in >> operation >> start >> length;
        bool set = operation == "B";
        start += SIZE / 2;
        tree.set(start, start + length, set);
        auto result = tree.get();
        out << result.first << " " << result.second << std::endl;
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}