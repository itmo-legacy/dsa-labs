#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>

const std::string taskname = "sparse";

constexpr int CAP = 16714589;

struct SparseTable {
    std::vector<std::vector<int>> table;

    explicit SparseTable(std::vector<int> const &vector) {
        table.emplace_back(vector);
        size_t size = 2;
        while (size < vector.size()) {
            std::vector<int> new_layer(table.back().size() - size / 2);
            for (std::size_t i = 0; i < new_layer.size(); ++i) {
                new_layer[i] = std::min(table.back()[i], table.back()[i + size / 2]);
            }
            table.emplace_back(new_layer);
            size *= 2;
        }
    }

    int get(size_t l, size_t r) {
        size_t length = r - l;
        size_t size = 1U << (table.size() - 1);
        auto layer_it = table.rbegin();
        while (size > length) {
            ++layer_it;
            size /= 2;
        }
        return std::min(layer_it->at(l), layer_it->at(r - size));
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    int a;
    in >> n >> m >> a;
    std::vector<int> as(n);
    as[0] = a;
    for (std::size_t i = 1; i < n; ++i) {
        as[i] = (23 * as[i - 1] + 21563) % CAP;
    }
    SparseTable table(as);

    size_t u, v;
    in >> u >> v;
    int answer = table.get(std::min(u, v) - 1, std::max(u, v));
    for (std::size_t i = 1; i < m; ++i) {
        u = (17 * u + 751 + answer + 2 * i) % n + 1;
        v = (13 * v + 593 + answer + 5 * i) % n + 1;
        answer = table.get(std::min(u, v) - 1, std::max(u, v));
    }
    out << u << " " << v << " " << answer << std::endl;
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}

