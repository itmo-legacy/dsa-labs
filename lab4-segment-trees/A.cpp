#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>

constexpr int CAP1 = 1U << 16U;
constexpr int CAP2 = 1U << 30U;

const std::string taskname = "sum0";

void solve(std::istream &in, std::ostream &out) {
    std::size_t n;
    unsigned short x, y, a0;
    in >> n >> x >> y >> a0;
    std::vector<long long> sums(n + 1, 0);
    for (std::size_t i = 1; i < n + 1; ++i) {
        sums[i] = sums[i - 1] + a0;
        a0 = a0 * x + y;
    }
    std::size_t m;
    int z, t, b0;
    in >> m >> z >> t >> b0;

    long long b = b0;
    long long result = 0;
    for (std::size_t i = 0; i < m; ++i) {
        int c1 = b % n;
        b = (z * b + t) % CAP2;
        int c2 = b % n;
        b = (z * b + t) % CAP2;
        result += sums[std::max(c1, c2) + 1] - sums[std::min(c1, c2)];
    }

    out << result << std::endl;
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }

    return 0;
}
