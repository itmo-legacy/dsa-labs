#include <fstream>
#include <cstring>
#include <iostream>
#include <vector>
#include <algorithm>
#include <tuple>
#include <experimental/optional>
#include <cmath>
#include <random>

using std::experimental::optional;

const std::string taskname = "permutation";

constexpr size_t SIZE = 100'001;


long translation_table[SIZE] = {0};

long sums[SIZE] = {0};
long random_sums[SIZE] = {0};


void compute_parameters() {
    std::mt19937_64 rng(std::random_device{}());
    std::uniform_int_distribution<std::mt19937_64::result_type> distribution{};

    for (long i = 1; i < SIZE; ++i) {
        translation_table[i] = distribution(rng);
        sums[i] = sums[i - 1] + i;
        random_sums[i] = random_sums[i - 1] + translation_table[i];
    }

}

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct SegmentTree {
    struct Value {
        long sum;
        long random_sum;

        explicit Value(int value)
                : sum(value), random_sum(translation_table[value]) {}

        Value(long sum, long random_sum)
                : sum(sum), random_sum(random_sum) {}

    };

    struct ValueM : public Monoid<Value> {
        static const Value MEMPTY() { return Value{0, 0}; }

        static Value MAPPEND(Value const &one, Value const &two) {
            return {one.sum + two.sum, one.random_sum + two.random_sum};
        }
    };

    using Adjustment = optional<int>;

    struct AdjustmentM : public Monoid<Adjustment> {
        static Adjustment MAPPEND(Adjustment const &one, Adjustment const &two) { return two ? two : one; }
    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        Value value;
        Adjustment adjustment = AdjustmentM::MEMPTY();

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        Value get(size_t l, size_t r) {
            if (r <= lbound or rbound <= l) { return ValueM::MEMPTY(); }
            push();
            if (l <= lbound and rbound <= r) { return value; }
            else { return {ValueM::MAPPEND(lchild->get(l, r), rchild->get(l, r))}; }
        };

        void push() {
            if (not is_leaf()) {
                lchild->adjustment = AdjustmentM::MAPPEND(lchild->adjustment, adjustment);
                rchild->adjustment = AdjustmentM::MAPPEND(rchild->adjustment, adjustment);
                update();
            } else {
                value = adjusted();
                adjustment = AdjustmentM::MEMPTY();
            }
        }

        void update() {
            value = ValueM::MAPPEND(get_value(lchild), get_value(rchild));
            adjustment = AdjustmentM::MEMPTY();
        }

        Value adjusted() {
            if (adjustment) {
                return Value(adjustment.value());
            } else {
                return value;
            }
        }

        friend Value get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->adjusted(); }
    };

    static void
    change(Node *&node, size_t left_interval_bound, size_t right_interval_bound, Adjustment const &adjustment) {
        if (right_interval_bound <= node->lbound or node->rbound <= left_interval_bound) { return; }
        node->push();
        if (left_interval_bound <= node->lbound and node->rbound <= right_interval_bound) {
            node->adjustment = AdjustmentM::MAPPEND(node->adjustment, adjustment);
        } else {
            change(node->lchild, left_interval_bound, right_interval_bound, adjustment);
            change(node->rchild, left_interval_bound, right_interval_bound, adjustment);
            node->update();
        }
    }

    Node *root = nullptr;

    explicit SegmentTree(std::vector<int> const &nums) {
        std::vector<Value> vector;
        vector.reserve(nums.size());
        for (int num : nums) { vector.emplace_back(num); }
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<Value> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    void update(size_t lbound, size_t rbound, int value) { change(root, lbound, rbound, {value}); }

    bool get(size_t l, size_t r) {
        auto result = root->get(l, r);
        long len = r - l;
        return result.sum == sums[len] and result.random_sum == random_sums[len];
    }
};

void solve(std::istream &in, std::ostream &out) {
    compute_parameters();
    size_t n;
    in >> n;
    std::vector<int> vector(n);
    for (std::size_t i = 0; i < n; ++i) {
        in >> vector[i];
    }
    SegmentTree tree(vector);
    size_t m;
    in >> m;
    for (size_t _ = 0; _ < m; ++_) {
        int instruction;
        in >> instruction;
        if (instruction == 1) {
            size_t place;
            int value;
            in >> place >> value;
            tree.update(place - 1, place, value);
        } else {
            size_t l, r;
            in >> l >> r;
            out << (tree.get(l - 1, r) ? "YES" : "NO") << "\n";
        }
    }
    out.flush();
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}