#include <limits>
#include <cstring>
#include <fstream>
#include <iostream>
#include <vector>
#include <experimental/optional>

const std::string taskname = "crypto";

template<typename T>
constexpr T middle(T left, T right) {
    return (left + right) >> 1U;
}

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

int cap = 0;

struct SegmentTree {
    using ValueT = std::vector<int>;

    struct ValueM : public Monoid<ValueT> {
        static const ValueT MEMPTY() {
            return {1, 0, 0, 1};
        }

        static ValueT MAPPEND(ValueT const &one, ValueT const &two) {
            int cell11 = one[0] * two[0] + one[1] * two[2];
            int cell12 = one[0] * two[1] + one[1] * two[3];
            int cell21 = one[2] * two[0] + one[3] * two[2];
            int cell22 = one[2] * two[1] + one[3] * two[3];
            return {cell11 % cap, cell12 % cap, cell21 % cap, cell22 % cap};
        }

    };

    struct Node {
        size_t lbound, rbound;
        Node *lchild, *rchild;
        ValueT value;

        constexpr size_t length() { return rbound - lbound; }

        constexpr bool is_leaf() { return length() == 1; }

        ValueT get(size_t left_interval_bound, size_t right_interval_bound) {
            if (right_interval_bound <= lbound or rbound <= left_interval_bound) { return ValueM::MEMPTY(); }
            if (left_interval_bound <= lbound && rbound <= right_interval_bound) { return value; }
            return ValueM::MAPPEND(lchild->get(left_interval_bound, right_interval_bound),
                                   rchild->get(left_interval_bound, right_interval_bound));
        }

        void update() { value = ValueM::MAPPEND(get_value(lchild), get_value(rchild)); }

        friend ValueT get_value(Node *node) { return node == nullptr ? ValueM::MEMPTY() : node->value; }
    };

    Node *root = nullptr;

    SegmentTree(int cap, std::vector<ValueT> const &vector) {
        ::cap = cap;
        build(root, 0, vector.size(), vector);
    }

    void build(Node *&node, size_t lbound, size_t rbound, std::vector<ValueT> const &vector) {
        if (rbound - lbound == 0) { return; }
        if (rbound - lbound == 1) {
            node = new Node{lbound, rbound, nullptr, nullptr, vector[lbound]};
        } else {
            size_t m = middle(lbound, rbound);
            Node *lchild = nullptr;
            build(lchild, lbound, m, vector);
            Node *rchild = nullptr;
            build(rchild, m, rbound, vector);
            combine(node, lchild, rchild);
        }
    }

    void combine(Node *&node, Node *lchild, Node *rchild) {
        node = new Node{lchild->lbound, rchild->rbound, lchild, rchild,
                        ValueM::MAPPEND(get_value(lchild), get_value(rchild))};
        node->update();
    }

    ValueT get(size_t l, size_t r) {
        return root->get(l, r);
    }
};

using ValueT = SegmentTree::ValueT;

std::istream &operator>>(std::istream &s, ValueT &value) {
    return s >> value[0] >> value[1] >> value[2] >> value[3];
}

std::ostream &operator<<(std::ostream &s, ValueT const &value) {
    return s << value[0] << " " << value[1] << "\n"
             << value[2] << " " << value[3];
}

void solve(std::istream &in, std::ostream &out) {
    int cap;
    size_t n, m;
    in >> cap >> n >> m;
    std::vector<ValueT> vector(n, std::vector<int>(4));
    for (std::size_t i = 0; i < n; ++i) {
        in >> vector[i];
    }
    SegmentTree tree(cap, vector);
    for (std::size_t i = 0; i < m; ++i) {
        size_t l, r;
        in >> l >> r;
        out << tree.get(l - 1, r) << "\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
    }
    return 0;
}