#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

int compare(double d1, double d2) {
    static constexpr double EPS = 1e-8;
    if (d1 - d2 <= -EPS) {
        return -1;
    }
    if (d1 - d2 >= EPS) {
        return +1;
    }
    return 0;
}

bool eq(double d1, double d2) {
    return compare(d1, d2) == 0;
}

bool gt(double d1, double d2) {
    return compare(d1, d2) > 0;
}

bool le(double d1, double d2) {
    return compare(d1, d2) <= 0;
}

using Capacity = double;
using Flow     = std::vector<double>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, double flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge *edge(EID eid) {
        return &edges.at(eid);
    }
};

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    Edge *edge(EID eid) {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, cap, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    double get_flow_value(Flow const &flow) {
        double value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_max_flow() {
        Flow flow(edges.size(), 0);
        while (true) {
            auto value = add_blocking_flow(flow);
            if (eq(value, 0)) {
                return flow;
            }
        }
    }

    std::vector<Edge const *> find_min_cut() {
        auto max_flow = find_max_flow();
        std::set<VID> left_dole;
        gather(s, left_dole, max_flow);
        std::vector<Edge const *> cut;
        for (EID eid = 0; eid < edges.size(); ++eid) {
            auto *edgep = edge(eid);
            if (left_dole.count(edgep->a) and not left_dole.count(edgep->b)) {
                cut.emplace_back(edgep);
            }
        }
        return cut;
    }

    void gather(VID v, std::set<VID> &dole, Flow const &flow) {
        if (dole.count(v)) {
            return;
        }
        dole.emplace(v);
        for (auto &&edge : vertices[v].edges) {
            assert(edge.a == v);
            if (not eq(edge.cap, flow[edge.id])) {
                std::cerr << edge.cap << ":" << flow[edge.id] << std::endl;
                gather(edge.b, dole, flow);
            }
        }
    }

    double add_blocking_flow(Flow &flow) {
        auto dist = build_distances(flow);
        if (dist[t] == MAX<unsigned>) {
            return false;
        }
        std::vector<size_t> first_edge(vertices.size(), 0);
        return dfs(s, MAX<double>, flow, first_edge, dist);
    }

    std::vector<unsigned> build_distances(Flow const &flow) {
        std::vector<unsigned> dist(vertices.size(), MAX<unsigned>);
        dist[s]               = 0;
        std::deque<VID> queue = {s};
        while (not queue.empty() and dist[t] == MAX<unsigned>) {
            auto first = queue.front();
            queue.pop_front();
            for (auto &&edge : vertices[first].edges) {
                if (dist[edge.b] != MAX<unsigned> or eq(edge.cap, flow[edge.id])) {
                    continue;
                }
                dist[edge.b] = dist[edge.a] + 1;
                queue.push_back(edge.b);
            }
        }
        return dist;
    }

    double dfs(VID vid, double flow_v, Flow &flow,
               std::vector<size_t> &first_edge,
               std::vector<unsigned> const &dist) {
        if (le(flow_v, 0)) {
            return 0;
        }
        if (vid == t) {
            return flow_v;
        }
        auto &v = vertices[vid];
        for (size_t &edge_i = first_edge[vid]; edge_i != v.edges.size(); ++edge_i) {
            auto &edge = v.edges[edge_i];
            assert(edge.a == vid);
            if (dist[edge.b] != dist[edge.a] + 1) {
                continue;
            }
            auto adj_flow_v = std::min(flow_v, edge.cap - flow[edge.id]);
            auto res_flow_v = dfs(edge.b, adj_flow_v, flow, first_edge, dist);
            if (gt(res_flow_v, 0)) {
                adjust_flow(flow, edge.id, edge.conj_id, res_flow_v);
                return res_flow_v;
            }
        }
        return false;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    VID s = 0, t = n - 1;
    Network network(n, s, t);
    for (size_t i = 0; i < m; ++i) {
        VID a, b;
        Capacity c;
        in >> a >> b >> c;
        network.add_edge(--a, --b, c);
    }
    auto min_cut = network.find_min_cut();
    auto value   = std::accumulate(WHOLE(min_cut), 0, [](auto acc, auto ep) { return acc + ep->cap; });
    out << min_cut.size() << " " << value << std::endl;
    std::sort(WHOLE(min_cut), [](auto ep1, auto ep2) { return ep1->id < ep2->id; });
    for (auto &&ep : min_cut) {

        out << ep->id / 2 + 1 << " ";
    }
    out << std::endl;
}

int main() { solve(std::cin, std::cout); }
