#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
3 3 1 3
1 2
1 3
2 3
)";
    auto output = R"(
YES
1 2 3 
1 3
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
2 1 1 2
1 2
)";
    auto output = R"(
NO
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
2 4 1 2
1 2
1 2
2 2
1 1
)";
    auto output = R"(
YES
1 2 
1 2
)";
    interact(input, output);
}

TEST(correctness, trivial3) {
    auto input  = R"(
4 6 1 4
1 2
2 3
3 2
1 3
2 4
3 4
)";
    auto output = R"(
YES
1 2 4 
1 3 4
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
