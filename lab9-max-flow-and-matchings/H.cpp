#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

int compare(double d1, double d2) {
    static constexpr double EPS = 1e-7;
    if (d1 - d2 <= -EPS) {
        return -1;
    }
    if (d1 - d2 >= EPS) {
        return +1;
    }
    return 0;
}

bool eq(double d1, double d2) {
    return compare(d1, d2) == 0;
}

bool lt(double d1, double d2) {
    return compare(d1, d2) < 0;
}

bool gt(double d1, double d2) {
    return compare(d1, d2) > 0;
}

bool le(double d1, double d2) {
    return compare(d1, d2) <= 0;
}

using Capacity = double;
using Flow     = std::vector<double>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, double flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge *edge(EID eid) {
        return &edges.at(eid);
    }
};

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) {
        extend(n, s, t);
    }

    void extend(size_t dn, VID new_s, VID new_t) {
        s = new_s;
        t = new_t;
        vertices.resize(vertices.size() + dn);
        for (size_t i = vertices.size() - dn; i < vertices.size(); ++i) {
            vertices[i].id = i;
        }
    }

    Edge *edge(EID eid) {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, 0, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    double get_flow_value(Flow const &flow) {
        double value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_max_flow() {
        Flow flow(edges.size(), 0);
        while (true) {
            auto value = add_blocking_flow(flow);
            if (eq(value, 0)) {
                return flow;
            }
        }
    }

    auto get_routes(Flow flow, size_t routes_count, size_t points_count) {
        std::vector<std::vector<VID>> routes(routes_count, {s});
        assert(vertices.size() % points_count == 0);
        size_t days_count = vertices.size() / points_count;
        std::vector<bool> marked(flow.size(), false);
        for (size_t day = 1; day < days_count + 1; ++day) {
            for (auto &route : routes) {
                assert(not route.empty());
                auto last_vid = route.back();
                for (auto &&edge : vertices[last_vid].edges) {
                    if (le(flow[edge.id], 0)) {
                        continue;
                    }
                    --flow[edge.id];
                    route.emplace_back(edge.b);
                    break;
                }
            }
        }
        return routes;
    }

    double add_blocking_flow(Flow &flow) {
        auto dist = build_distances(flow);
        if (dist[t] == MAX<unsigned>) {
            return 0;
        }
        std::vector<size_t> first_edge(vertices.size(), 0);
        return dfs(s, MAX<double>, flow, first_edge, dist);
    }

    std::vector<unsigned> build_distances(Flow const &flow) {
        std::vector<unsigned> dist(vertices.size(), MAX<unsigned>);
        dist[s]               = 0;
        std::deque<VID> queue = {s};
        while (not queue.empty() and dist[t] == MAX<unsigned>) {
            auto first = queue.front();
            queue.pop_front();
            for (auto &&edge : vertices[first].edges) {
                if (dist[edge.b] != MAX<unsigned> or eq(edge.cap, flow[edge.id])) {
                    continue;
                }
                dist[edge.b] = dist[edge.a] + 1;
                queue.push_back(edge.b);
            }
        }
        return dist;
    }

    double dfs(VID vid, double flow_v, Flow &flow, std::vector<size_t> &first_edge, std::vector<unsigned> const &dist) {
        if (le(flow_v, 0)) {
            return 0;
        }
        if (vid == t) {
            return flow_v;
        }
        auto &v = vertices[vid];
        for (size_t &edge_i = first_edge[vid]; edge_i != v.edges.size(); ++edge_i) {
            auto &edge = v.edges[edge_i];
            assert(edge.a == vid);
            if (dist[edge.b] != dist[edge.a] + 1) {
                continue;
            }
            auto adj_flow_v = std::min(flow_v, edge.cap - flow[edge.id]);
            auto res_flow_v = dfs(edge.b, adj_flow_v, flow, first_edge, dist);
            if (gt(res_flow_v, 0)) {
                adjust_flow(flow, edge.id, edge.conj_id, res_flow_v);
                return res_flow_v;
            }
        }
        return false;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m, k, s, t;
    in >> n >> m >> k >> s >> t;
    --s, --t;
    std::vector<std::vector<VID>> roads_from(n);
    while (m--) {
        VID a, b;
        in >> a >> b;
        --a, --b;
        roads_from[a].emplace_back(b);
        roads_from[b].emplace_back(a);
    }
    Network network(n, s, t);
    size_t i = 0;
    Flow flow;
    double flow_value;
    do {
        auto today    = n * i;
        auto tomorrow = today + n;
        network.extend(n, s, tomorrow + t);
        for (VID u = 0; u < n; ++u) {
            network.add_edge(today + u, tomorrow + u, MAX<Capacity>);
            for (VID next : roads_from[u]) {
                network.add_edge(today + u, tomorrow + next, 1);
            }
        }
        flow       = network.find_max_flow();
        flow_value = network.get_flow_value(flow);
        ++i;
    } while (lt(flow_value, k));

    auto routes = network.get_routes(flow, k, n);
    assert(routes.size() == k);
    out << i << std::endl;
    for (size_t day = 1; day < i + 1; ++day) {
        std::vector<std::pair<size_t, VID>> moves;
        for (size_t route_i = 0; route_i < k; ++route_i) {
            auto &route = routes[route_i];
            if (route[day] % n != route[day - 1] % n) {
                moves.emplace_back(route_i, route[day] % n);
            }
        }
        out << moves.size();
        for (auto &&[ship, planet] : moves) {
            out << "  " << ship + 1 << " " << planet + 1;
        }
        out << std::endl;
    }
}

int main() {
    std::ifstream fin("bring.in");
    std::ofstream fout("bring.out");
    solve(fin, fout);
}
