#include <algorithm>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <deque>
#include <iomanip>
#include <iostream>
#include <queue>
#include <thread>
#include <type_traits>

using std::size_t;

int compare(double d1, double d2) {
    static constexpr double EPS = 1e-8;
    if (d1 - d2 <= -EPS) {
        return -1;
    }
    if (d1 - d2 >= EPS) {
        return +1;
    }
    return 0;
}

bool eq(double d1, double d2) {
    return compare(d1, d2) == 0;
}

bool gt(double d1, double d2) {
    return compare(d1, d2) > 0;
}

bool le(double d1, double d2) {
    return compare(d1, d2) <= 0;
}

using Capacity = double;
using Flow     = std::vector<double>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, double flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    void add_arc(VID b, Capacity cap, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, eid, conj_eid});
    }
};

struct Network {
    std::vector<Vertex> vertices;
    size_t arcs_count = 0;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    void add_edge(VID a, VID b, Capacity cap) {
        EID one = arcs_count++;
        EID two = arcs_count++;
        vertices[a].add_arc(b, cap, one, two);
        vertices[b].add_arc(a, cap, two, one);
    }

    double get_flow_value(Flow const &flow) {
        double value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_max_flow() {
        Flow flow(arcs_count, 0);
        while (true) {
            auto value = add_blocking_flow(flow);
            if (eq(value, 0)) {
                return flow;
            }
        }
    }

    double add_blocking_flow(Flow &flow) {
        auto dist = build_distances(flow);
        if (dist[t] == MAX<unsigned>) {
            return false;
        }
        std::vector<size_t> first_edge(vertices.size(), 0);
        return dfs(s, MAX<double>, flow, first_edge, dist);
    }

    std::vector<unsigned> build_distances(Flow const &flow) {
        std::vector<unsigned> dist(vertices.size(), MAX<unsigned>);
        dist[s]               = 0;
        std::deque<VID> queue = {s};
        while (not queue.empty() and dist[t] == MAX<unsigned>) {
            auto first = queue.front();
            queue.pop_front();
            for (auto &&edge : vertices[first].edges) {
                if (dist[edge.b] != MAX<unsigned> or eq(edge.cap, flow[edge.id])) {
                    continue;
                }
                dist[edge.b] = dist[edge.a] + 1;
                queue.push_back(edge.b);
            }
        }
        return dist;
    }

    double dfs(VID vid, double flow_v, Flow &flow,
               std::vector<size_t> &first_edge,
               std::vector<unsigned> const &dist) {
        if (le(flow_v, 0)) {
            return 0;
        }
        if (vid == t) {
            return flow_v;
        }
        auto &v = vertices[vid];
        for (size_t &edge_i = first_edge[vid]; edge_i != v.edges.size(); ++edge_i) {
            auto &edge = v.edges[edge_i];
            assert(edge.a == vid);
            if (dist[edge.b] != dist[edge.a] + 1) {
                continue;
            }
            auto adj_flow_v = std::min(flow_v, edge.cap - flow[edge.id]);
            auto res_flow_v = dfs(edge.b, adj_flow_v, flow, first_edge, dist);
            if (gt(res_flow_v, 0)) {
                adjust_flow(flow, edge.id, edge.conj_id, res_flow_v);
                return res_flow_v;
            }
        }
        return false;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    VID s = 0, t = n - 1;
    Network network(n, s, t);
    while (m--) {
        VID a, b;
        Capacity c;
        in >> a >> b >> c;
        --a, --b;
        network.add_edge(a, b, c);
    }
    auto max_flow = network.find_max_flow();
    double value  = network.get_flow_value(max_flow);
    out << std::setprecision(8) << value << std::endl;
    for (size_t i = 0; i < max_flow.size(); i += 2) {
        out << std::setprecision(8) << max_flow[i] << std::endl;
    }
}

int main() { solve(std::cin, std::cout); }
