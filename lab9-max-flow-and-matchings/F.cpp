#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>

#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

int compare(double d1, double d2) {
    static constexpr double EPS = 1e-7;
    if (d1 - d2 <= -EPS) {
        return -1;
    }
    if (d1 - d2 >= EPS) {
        return +1;
    }
    return 0;
}

bool eq(double d1, double d2) {
    return compare(d1, d2) == 0;
}

bool lt(double d1, double d2) {
    return compare(d1, d2) < 0;
}

bool gt(double d1, double d2) {
    return compare(d1, d2) > 0;
}

bool le(double d1, double d2) {
    return compare(d1, d2) <= 0;
}

using Capacity = double;
using Flow     = std::vector<double>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, double flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge *edge(EID eid) {
        return &edges.at(eid);
    }
};

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    Edge *edge(EID eid) {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, 0, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    double get_flow_value(Flow const &flow) {
        double value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_max_flow() {
        Flow flow(edges.size(), 0);
        while (true) {
            auto value = add_blocking_flow(flow);
            if (eq(value, 0)) {
                return flow;
            }
        }
    }

    double add_blocking_flow(Flow &flow) {
        auto dist = build_distances(flow);
        if (dist[t] == MAX<unsigned>) {
            return 0;
        }
        std::vector<size_t> first_edge(vertices.size(), 0);
        return dfs(s, MAX<double>, flow, first_edge, dist);
    }

    std::vector<unsigned> build_distances(Flow const &flow) {
        std::vector<unsigned> dist(vertices.size(), MAX<unsigned>);
        dist[s]               = 0;
        std::deque<VID> queue = {s};
        while (not queue.empty() and dist[t] == MAX<unsigned>) {
            auto first = queue.front();
            queue.pop_front();
            for (auto &&edge : vertices[first].edges) {
                if (dist[edge.b] != MAX<unsigned> or eq(edge.cap, flow[edge.id])) {
                    continue;
                }
                dist[edge.b] = dist[edge.a] + 1;
                queue.push_back(edge.b);
            }
        }
        return dist;
    }

    double dfs(VID vid, double flow_v, Flow &flow,
               std::vector<size_t> &first_edge,
               std::vector<unsigned> const &dist) {
        if (le(flow_v, 0)) {
            return 0;
        }
        if (vid == t) {
            return flow_v;
        }
        auto &v = vertices[vid];
        for (size_t &edge_i = first_edge[vid]; edge_i != v.edges.size(); ++edge_i) {
            auto &edge = v.edges[edge_i];
            assert(edge.a == vid);
            if (dist[edge.b] != dist[edge.a] + 1) {
                continue;
            }
            auto adj_flow_v = std::min(flow_v, edge.cap - flow[edge.id]);
            auto res_flow_v = dfs(edge.b, adj_flow_v, flow, first_edge, dist);
            if (gt(res_flow_v, 0)) {
                adjust_flow(flow, edge.id, edge.conj_id, res_flow_v);
                return res_flow_v;
            }
        }
        return false;
    }
};

template <typename T>
constexpr T sqr(T a) { return a * a; }

struct Point {
    double x, y;

    double distance(Point const &other) {
        return sqrt(sqr(x - other.x) + sqr(y - other.y));
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<Point> left_dole(n);
    std::vector<double> speeds(n);
    std::vector<std::vector<Capacity>> vectors(n, std::vector<Capacity>(n));
    std::vector<Capacity> sorted_vectors(n * n);
    for (size_t i = 0; i < n; ++i) {
        in >> left_dole[i].x >> left_dole[i].y >> speeds[i];
    }
    for (size_t i = 0; i < n; ++i) {
        Point fin;
        in >> fin.x >> fin.y;
        for (size_t j = 0; j < n; ++j) {
            vectors[j][i]             = fin.distance(left_dole[j]) / speeds[j];
            sorted_vectors[j * n + i] = vectors[j][i];
        }
    }
    std::sort(WHOLE(sorted_vectors));
    std::for_each(WHOLE(sorted_vectors), [](auto &&el) { std::cerr << el << " "; });
    std::cerr << std::endl;
    VID s = 2 * n,
        t = 2 * n + 1;
    for (auto &&width : sorted_vectors) {
        Network network(2 * n + 2, s, t);
        for (size_t i = 0; i < n; ++i) {
            for (size_t j = 0; j < n; ++j) {
                if (le(vectors[i][j], width)) {
                    network.add_edge(i, j + n, 1);
                }
            }
        }
        for (size_t i = 0; i < n; ++i) {
            network.add_edge(s, i, 1);
            network.add_edge(i + n, t, 1);
        }
        auto flow       = network.find_max_flow();
        auto flow_value = network.get_flow_value(flow);
        if (eq(flow_value, n)) {
            out << std::setprecision(12) << width << std::endl;
            return;
        }
    }
    assert(false && "shit");
}

int main() { solve(std::cin, std::cout); }
