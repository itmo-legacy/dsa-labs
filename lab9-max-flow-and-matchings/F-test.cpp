#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
2
0 0 1
0 1 1
1 1
1 0
)";
    auto output = R"(
1
)";
    interact(input, output);
}

TEST(correctness, example2) {
    auto input  = R"(
2
0 0 1
0 1 1
1 1
2 1
)";
    auto output = R"(
2
)";
    interact(input, output);
}

TEST(correctness, example3) {
    auto input  = R"(
2
0 0 1
5 0 1
5 12
10 12
)";
    auto output = R"(
13
)";
    interact(input, output);
}

TEST(correctness, example4) {
    auto input  = R"(
2
0 0 2
5 0 1
5 12
10 12
)";
    auto output = R"(
12
)";
    interact(input, output);
}

TEST(correctness, example5) {
    auto input  = R"(
4
78 520 5
827 239 5
620 200 7
809 269 7
986 496
754 745
772 375
44 223
)";
    auto output = R"(
68.452426501
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
