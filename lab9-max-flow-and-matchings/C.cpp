#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

int compare(double d1, double d2) {
    static constexpr double EPS = 1e-8;
    if (d1 - d2 <= -EPS) {
        return -1;
    }
    if (d1 - d2 >= EPS) {
        return +1;
    }
    return 0;
}

bool eq(double d1, double d2) {
    return compare(d1, d2) == 0;
}

bool lt(double d1, double d2) {
    return compare(d1, d2) < 0;
}

bool gt(double d1, double d2) {
    return compare(d1, d2) > 0;
}

bool le(double d1, double d2) {
    return compare(d1, d2) <= 0;
}

using Capacity = double;
using Flow     = std::vector<double>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, double flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge *edge(EID eid) {
        return &edges.at(eid);
    }
};

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    Edge *edge(EID eid) {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, 0, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    double get_flow_value(Flow const &flow) {
        double value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_max_flow() {
        Flow flow(edges.size(), 0);
        while (true) {
            auto value = add_blocking_flow(flow);
            if (eq(value, 0)) {
                return flow;
            }
        }
    }

    auto find_two_paths() {
        std::vector<VID> path1;
        std::vector<VID> path2;
        auto flow = find_max_flow();
        std::vector<bool> marked(edges.size(), false);
        for (auto &edge : vertices[s].edges) {
            if (gt(flow[edge.id], 0)) {
                auto &path = path1.empty() ? path1 : path2;
                assert(find_path(edge.b, path, flow, marked));
                path.emplace_back(s);
                std::reverse(WHOLE(path));
                if (not path1.empty() and not path2.empty()) {
                    break;
                }
            }
        }
        return std::make_pair(path1, path2);
    }

    bool find_path(VID v, std::vector<VID> &path, Flow const &flow, std::vector<bool> &marked) {
        if (v == t) {
            path.emplace_back(v);
            return true;
        }
        for (auto &&edge : vertices[v].edges) {
            if (not gt(flow[edge.id], 0) or marked[edge.id]) {
                continue;
            }
            marked[edge.id] = true;
            bool ok         = find_path(edge.b, path, flow, marked);
            if (ok) {
                path.emplace_back(v);
                return true;
            }
        }
        return false;
    }

    double add_blocking_flow(Flow &flow) {
        auto dist = build_distances(flow);
        if (dist[t] == MAX<unsigned>) {
            return 0;
        }
        std::vector<size_t> first_edge(vertices.size(), 0);
        return dfs(s, MAX<double>, flow, first_edge, dist);
    }

    std::vector<unsigned> build_distances(Flow const &flow) {
        std::vector<unsigned> dist(vertices.size(), MAX<unsigned>);
        dist[s]               = 0;
        std::deque<VID> queue = {s};
        while (not queue.empty() and dist[t] == MAX<unsigned>) {
            auto first = queue.front();
            queue.pop_front();
            for (auto &&edge : vertices[first].edges) {
                if (dist[edge.b] != MAX<unsigned> or eq(edge.cap, flow[edge.id])) {
                    continue;
                }
                dist[edge.b] = dist[edge.a] + 1;
                queue.push_back(edge.b);
            }
        }
        return dist;
    }

    double dfs(VID vid, double flow_v, Flow &flow,
               std::vector<size_t> &first_edge,
               std::vector<unsigned> const &dist) {
        if (le(flow_v, 0)) {
            return 0;
        }
        if (vid == t) {
            return flow_v;
        }
        auto &v = vertices[vid];
        for (size_t &edge_i = first_edge[vid]; edge_i != v.edges.size(); ++edge_i) {
            auto &edge = v.edges[edge_i];
            assert(edge.a == vid);
            if (dist[edge.b] != dist[edge.a] + 1) {
                continue;
            }
            auto adj_flow_v = std::min(flow_v, edge.cap - flow[edge.id]);
            auto res_flow_v = dfs(edge.b, adj_flow_v, flow, first_edge, dist);
            if (gt(res_flow_v, 0)) {
                adjust_flow(flow, edge.id, edge.conj_id, res_flow_v);
                return res_flow_v;
            }
        }
        return false;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    VID s, t;
    in >> n >> m >> s >> t;
    Network network(n, s - 1, t - 1);
    for (size_t i = 0; i < m; ++i) {
        VID a, b;
        in >> a >> b;
        network.add_edge(a - 1, b - 1, 1);
    }
    auto paths = network.find_two_paths();
    if (paths.first.empty() or paths.second.empty()) {
        out << "NO" << std::endl;
    } else {
        out << "YES" << std::endl;
        for (auto &&v : paths.first) {
            out << v + 1 << " ";
        }
        out << std::endl;
        for (auto &&v : paths.second) {
            out << v + 1 << " ";
        }
        out << std::endl;
    }
}

int main() { solve(std::cin, std::cout); }
