#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

template <typename T>
static constexpr T MAX = std::numeric_limits<T>::max();

template <typename T>
static constexpr T MIN = std::numeric_limits<T>::min();

using AdjTable = std::vector<std::vector<unsigned>>;

struct Graph {
    AdjTable const table;
    size_t const n;

    Graph(AdjTable &&table) : table(table), n(table.size()) {}

    unsigned distance(size_t s, size_t t) {
        std::vector<unsigned> distances(n, MAX<int>);
        distances[s]  = 0;
        auto min_v    = s;
        auto min_dist = distances[s];
        std::vector<bool> used(n, false);
        while (min_dist != MAX<unsigned>) {
            used[min_v] = true;
            for (size_t u = 0; u < n; ++u) {
                distances[u] = std::min(distances[u], distances[min_v] + table[u][min_v]);
            }
            min_dist = MAX<unsigned>;
            for (size_t u = 0; u < n; ++u) {
                if (not used[u] and distances[u] < min_dist) {
                    min_dist = distances[u];
                    min_v    = u;
                }
            }
        }
        return distances[t];
    }
};

struct Rect {
    int x1, y1;
    int x2, y2;
};

unsigned distance(Rect const &a, Rect const &b) {
    auto &lefter  = a.x1 < b.x1 ? a : b;
    auto &righter = &lefter == &a ? b : a;
    auto &lower   = a.y1 < b.y1 ? a : b;
    auto &upper   = &lower == &a ? b : a;

    auto xdist = unsigned(std::max(0, righter.x1 - lefter.x2));
    auto ydist = unsigned(std::max(0, upper.y1 - lower.y2));

    return std::max(xdist, ydist);
}

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    int w;
    in >> n >> w;
    AdjTable table(n + 2, std::vector<unsigned>(n + 2, 0));
    std::vector<Rect> objects(n + 2);
    objects[0]  = Rect{MIN<int> / 2, 0, MAX<int> / 2, 0};
    objects[1]  = Rect{MIN<int> / 2, w, MAX<int> / 2, w};
    table[0][1] = table[1][0] = distance(objects[0], objects[1]);
    for (size_t i = 2; i < n + 2; ++i) {
        int x1, y1, x2, y2;
        in >> x1 >> y1 >> x2 >> y2;
        objects[i] = Rect{x1, y1, x2, y2};
        for (size_t j = 0; j < i; ++j) {
            table[j][i] = table[i][j] = distance(objects[j], objects[i]);
        }
    }
    Graph graph(std::move(table));
    out << graph.distance(0, 1) << std::endl;
}

int main() { solve(std::cin, std::cout); }
