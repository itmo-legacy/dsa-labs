#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
3 3
1 2 3
1 3 5
3 2 7
)";
    auto output = R"(
2 8
1 2
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
2 2
1 2 1
2 1 3
)";
    auto output = R"(
2 4
1 2
)";
    interact(input, output);
}

TEST(correctness, example2) {
    auto input  = R"(
7 10
1 2 7
1 3 7
1 4 7
2 3 7
2 5 7
3 6 7
4 7 7
5 4 7
5 6 7
6 7 7
)";
    auto output = R"(
2 14
7 10
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
