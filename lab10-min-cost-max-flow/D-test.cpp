#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
3 0 0
0 3 0
)";
    auto output = R"(
3
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
1 1 1
1 1 1
)";
    auto output = R"(
0
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
1 2 1
1 1 2
)";
    auto output = R"(
0
)";
    interact(input, output);
}

TEST(correctness, trivial3) {
    auto input  = R"(
1 3 1
1 1 3
)";
    auto output = R"(
1
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
