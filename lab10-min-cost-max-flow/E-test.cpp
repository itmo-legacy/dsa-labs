#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
3 3
30 25 30
1 2 3
2 3 5
3 1 10
)";
    auto output = R"(
18
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
3 3
2 25 30
1 2 3
2 3 5
3 1 10
)";
    auto output = R"(
18
)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
2 1
5 5
1 2 3
)";
    auto output = R"(
10
)";
    interact(input, output);
}

TEST(correctness, trivial3) {
    auto input  = R"(
2 2
5 5
1 2 3
2 1 6
)";
    auto output = R"(
9
)";
    interact(input, output);
}

TEST(correctness, trivial4) {
    auto input  = R"(
1 0
5
)";
    auto output = R"(
5
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
