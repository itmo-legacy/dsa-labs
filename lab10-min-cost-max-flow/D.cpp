#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr unsigned NPOS = unsigned(-1);

using Capacity = std::int64_t;
using Cost     = std::int64_t;
using Flow     = std::vector<Capacity>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, Capacity flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    Cost cost;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, Cost cost, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, cost, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge const *edge(EID eid) const {
        return &edges.at(eid);
    }
};

struct Network;

struct DistInfo {
    Cost dist = MAX<Cost>;
    EID prev  = NPOS;
};

std::vector<DistInfo>
ford_bellman(Network const &graph, VID s);

template <typename WeightF>
std::vector<DistInfo>
dijkstra(Network const &graph, WeightF weight_f, VID s, Flow const &flow);

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    Edge const *edge(EID eid) const {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap, Cost cost) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, cost, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, 0, -cost, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    Capacity get_flow_value(Flow const &flow) {
        Capacity value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Flow find_min_cost_max_flow() {
        Flow flow(edges.size(), 0);
        // auto potency = ford_bellman(*this, s); // Johnson's potentials
        std::vector<DistInfo> potency(this->vertices.size(), DistInfo{0, 0});
        auto weight_f = [&potency, this](EID eid) {
                            auto edgep = edge(eid);
                            return edgep->cost + potency[edgep->a].dist - potency[edgep->b].dist ; };
        while (true) {
            potency = dijkstra(*this, weight_f, s, flow);
            if (potency[t].dist == MAX<Cost>) {
                break;
            }
            Capacity flow_v = MAX<Capacity>;
            for (VID v = t; v != s; v = edge(potency[v].prev)->a) {
                auto edgep = edge(potency[v].prev);
                flow_v     = std::min(flow_v, edgep->cap - flow[edgep->id]);
            }
            for (VID v = t; v != s; v = edge(potency[v].prev)->a) {
                auto edgep = edge(potency[v].prev);
                adjust_flow(flow, edgep->id, edgep->conj_id, flow_v);
            }
        }
        return flow;
    }
};

std::vector<DistInfo>
ford_bellman(Network const &graph, VID s) {
    size_t const n = graph.vertices.size();
    std::vector<DistInfo> infos(n);
    infos[s].dist = 0;
    for (size_t k = 0; k < n; ++k) {
        for (EID eid = 0; eid < graph.edges.size(); ++eid) {
            auto *edge = graph.edge(eid);
            if (edge->cap == 0) {
                continue;
            }
            if (infos[edge->a].dist == MAX<Cost>) {
                continue;
            }
            auto new_dist = infos[edge->a].dist + edge->cost;
            if (new_dist < infos[edge->b].dist) {
                infos[edge->b] = {new_dist, eid};
            }
        }
    }
    return infos;
}

template <typename WeightF>
std::vector<DistInfo>
dijkstra(Network const &graph, WeightF weight_f, VID s, Flow const &flow) {
    size_t const n = graph.vertices.size();
    std::vector<DistInfo> infos(n);
    infos[s].dist = 0;

    std::set<std::pair<Cost, VID>> unused = {{0, s}};
    while (not unused.empty()) {
        VID min_v = unused.begin()->second;
        unused.erase(unused.begin());
        for (auto &&edge : graph.vertices[min_v].edges) {
            if (edge.cap == flow[edge.id]) {
                continue;
            }
            VID u         = edge.b;
            auto new_dist = infos[min_v].dist + weight_f(edge.id);
            if (infos[u].dist > new_dist) {
                unused.erase({infos[u].dist, u});
                infos[u] = {new_dist, edge.id};
                unused.insert({infos[u].dist, u});
            }
        }
    }

    return infos;
}

void solve(std::istream &in, std::ostream &out) {
    constexpr Capacity CAP_CAP = 400'000'100;

    Capacity r1, s1, p1, r2, s2, p2;
    in >> r2 >> s2 >> p2 >> r1 >> s1 >> p1;
    Network network(6 + 2, 0, 7);
    network.add_edge(0, 1, r1, 0);
    network.add_edge(0, 2, s1, 0);
    network.add_edge(0, 3, p1, 0);

    network.add_edge(1, 4, CAP_CAP, 0);
    network.add_edge(1, 5, CAP_CAP, 0);
    network.add_edge(1, 6, CAP_CAP, 1);
    network.add_edge(2, 4, CAP_CAP, 1);
    network.add_edge(2, 5, CAP_CAP, 0);
    network.add_edge(2, 6, CAP_CAP, 0);
    network.add_edge(3, 4, CAP_CAP, 0);
    network.add_edge(3, 5, CAP_CAP, 1);
    network.add_edge(3, 6, CAP_CAP, 0);

    network.add_edge(4, 7, r2, 0);
    network.add_edge(5, 7, s2, 0);
    network.add_edge(6, 7, p2, 0);

    Flow flow = network.find_min_cost_max_flow();
    Cost cost = 0;
    for (EID eid = 0; eid < flow.size(); ++eid) {
        Edge const *edge = network.edge(eid);
        if (flow[eid] > 0) {
            cost += edge->cost * flow[eid];
        }
    }
    out << cost << std::endl;
}

int main() {
    //    solve(std::cin, std::cout);
    std::ifstream fin("rps2.in");
    std::ofstream fout("rps2.out");
    solve(fin, fout);
}
