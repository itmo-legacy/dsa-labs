#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

constexpr size_t NOP = size_t(-1);

using Capacity = std::int64_t;
using Cost     = std::int64_t;
using VID      = size_t;

template<typename T>
constexpr T MAX = std::numeric_limits<T>::max();

using Graph = std::vector<std::vector<Cost>>;

std::vector<std::vector<Cost>>
floyd(Graph const &graph) {
    const size_t n = graph.size();
    std::vector<std::vector<Cost>> dist = graph;
    for (VID w = 0; w < n; ++w) {
        for (VID u = 0; u < n; ++u) {
            for (VID v = 0; v < n; ++v) {
                if (dist[u][w] == MAX<Cost> or dist[w][v] == MAX<Cost>) {
                    continue;
                }
                dist[u][v] = std::min(dist[u][v], dist[u][w] + dist[w][v]);
            }
        }
    }
    return dist;
}

std::uint64_t
hungarian(Graph const &graph) {
    size_t n = graph.size();

    std::vector<Cost> potency_u(n, 0), potency_v(n, 0);
    std::vector<size_t> markIndices(n, NOP);

    for (size_t i = 0; i < n; i++) {
        std::vector<size_t > links(n, NOP);
        std::vector<Cost> mins(n, MAX<Cost>);
        std::vector<bool> visited(n, false);

        size_t markedI = i, markedJ = NOP, j = NOP;
        while (markedI != NOP) {
            j = NOP;
            for (size_t j1 = 0; j1 < n; j1++) {
                if (not visited[j1]) {
                    if (graph[markedI][j1] != MAX<Cost> and graph[markedI][j1] - potency_u[markedI] - potency_v[j1] < mins[j1]) {
                        mins[j1] = graph[markedI][j1] - potency_u[markedI] - potency_v[j1];
                        links[j1] = markedJ;
                    }
                    if (j == NOP or mins[j1] < mins[j]) {
                        j = j1;
                    }
                }
            }

            Cost delta = mins[j];
            for (size_t k = 0; k < n; k++) {
                if (visited[k]) {
                    potency_u[markIndices[k]] += delta;
                    potency_v[k] -= delta;
                } else {
                    mins[k] -= delta;
                }
            }
            potency_u[i] += delta;

            visited[j] = true;
            markedJ = j;
            markedI = markIndices[j];
        }

        for (; links[j] != NOP; j = links[j]) {
            markIndices[j] = markIndices[links[j]];
        }
        markIndices[j] = i;
    }

    std::uint64_t value = 0;
    for (size_t j = 0; j < n; j++) {
        if (markIndices[j] != NOP) {
            value += std::uint64_t(graph[markIndices[j]][j]);
        }
    }
    return value;
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    std::vector<std::tuple<VID, VID, Cost>> edges;
    for (size_t i = 0; i < n; ++i) {
        Cost loop_cost;
        in >> loop_cost;
        edges.emplace_back(i, i, loop_cost);
    }
    for (size_t i = 0; i < m; ++i) {
        VID u, v;
        Cost cost;
        in >> u >> v >> cost;
        edges.emplace_back(u - 1, v - 1, cost);
    }

    Graph graph(n, std::vector<Cost>(n, MAX<Cost>));
    for (auto &&[u, v, cost] : edges) {
        graph[u][v] = cost;
    }
    auto dist = floyd(graph);

    auto value = hungarian(dist);
    out << value << std::endl;
}

int main() { solve(std::cin, std::cout); }

