#include <algorithm>
#include <cassert>
#include <cmath>
#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <set>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;
constexpr unsigned NOP = unsigned(-1);

using Capacity = std::int64_t;
using Cost     = std::int64_t;
using Flow     = std::vector<Capacity>;
using VID      = size_t;
using EID      = size_t;

template <typename T>
constexpr T MAX = std::numeric_limits<T>::max();

void adjust_flow(Flow &flow, EID eid, EID conj_eid, Capacity flow_v) {
    flow[eid] += flow_v;
    flow[conj_eid] -= flow_v;
}

struct Edge {
    VID a, b;
    Capacity cap;
    Cost cost;
    EID id;
    EID conj_id;
};

struct Vertex {
    VID id;
    std::vector<Edge> edges;

    EID add_arc(VID b, Capacity cap, Cost cost, EID eid, EID conj_eid) {
        edges.push_back(Edge{id, b, cap, cost, eid, conj_eid});
        return edges.size() - 1;
    }

    Edge const *edge(EID eid) const {
        return &edges.at(eid);
    }

    Edge *edge(EID eid) {
        return &edges.at(eid);
    }
};

struct Network;

struct DistInfo {
    Cost dist = MAX<Cost>;
    EID prev  = NOP;
};

template <typename WeightF>
std::vector<DistInfo>
dijkstra(Network const &graph, WeightF weight_f, VID s, Flow const &flow);

struct Network {
    std::vector<Vertex> vertices;
    std::vector<std::pair<VID, EID>> edges;
    VID s, t;

    Network(size_t n, VID s, VID t) : vertices(n), s(s), t(t) {
        for (size_t i = 0; i < n; ++i) {
            vertices[i].id = i;
        }
    }

    Edge const *edge(EID eid) const {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    Edge *edge(EID eid) {
        auto [vid, local_eid] = edges[eid];
        return vertices[vid].edge(local_eid);
    }

    void add_edge(VID a, VID b, Capacity cap, Cost cost) {
        EID eid1       = edges.size();
        EID eid2       = edges.size() + 1;
        EID local_eid1 = vertices[a].add_arc(b, cap, cost, eid1, eid2);
        EID local_eid2 = vertices[b].add_arc(a, 0, -cost, eid2, eid1);
        edges.emplace_back(a, local_eid1);
        edges.emplace_back(b, local_eid2);
    }

    Capacity get_flow_value(Flow const &flow) {
        Capacity value = 0;
        for (auto &&edge : vertices[s].edges) {
            value += flow[edge.id];
        }
        return value;
    }

    Cost get_flow_cost(Flow const &flow) {
        Cost cost = 0;
        for (EID eid = 0; eid < flow.size(); ++eid) {
            Edge const *edgep = edge(eid);
            if (flow[eid] > 0) {
                cost += edgep->cost * flow[eid];
            }
        }
        return cost;
    }

    Flow find_min_cost_max_flow() {
        Flow flow(edges.size(), 0);
        maximize_flow(flow);
        return flow;
    }

    void maximize_flow(Flow &flow) {
        flow.resize(edges.size(), 0);
        std::vector<DistInfo> potency(this->vertices.size(), DistInfo{0, 0});
        auto weight_f = [&potency, this](EID eid) {
            auto edgep = edge(eid);
            return edgep->cost + potency[edgep->a].dist - potency[edgep->b].dist;
        };
        while (true) {
            potency = dijkstra(*this, weight_f, s, flow);
            if (potency[t].dist == MAX<Cost>) {
                break;
            }
            Capacity flow_v = MAX<Capacity>;
            for (VID v = t; v != s; v = edge(potency[v].prev)->a) {
                auto edgep = edge(potency[v].prev);
                flow_v     = std::min(flow_v, edgep->cap - flow[edgep->id]);
            }
            for (VID v = t; v != s; v = edge(potency[v].prev)->a) {
                auto edgep = edge(potency[v].prev);
                adjust_flow(flow, edgep->id, edgep->conj_id, flow_v);
            }
        }
    }
};

template <typename WeightF>
std::vector<DistInfo>
dijkstra(Network const &graph, WeightF weight_f, VID s, Flow const &flow) {
    size_t const n = graph.vertices.size();
    std::vector<DistInfo> infos(n);
    infos[s].dist = 0;
    auto min_v    = s;
    auto min_dist = infos[s].dist;
    std::vector<bool> used(n, false);
    while (min_dist != MAX<Cost>) {
        used[min_v] = true;
        for (auto &&edge : graph.vertices[min_v].edges) {
            if (edge.cap == flow[edge.id]) {
                continue;
            }
            auto weight = weight_f(edge.id);
            if (min_dist + weight < infos[edge.b].dist) {
                infos[edge.b] = {min_dist + weight, edge.id};
            }
        }
        min_dist = MAX<Cost>;
        for (VID v = 0; v < n; ++v) {
            if (not used[v] and infos[v].dist < min_dist) {
                min_dist = infos[v].dist;
                min_v    = v;
            }
        }
    }
    return infos;
}

using Color = unsigned;

using Graph = std::vector<std::vector<Cost>>;

std::uint64_t
hungarian(Graph const &graph) {
    if (graph.empty()) {
        return 0;
    }
    size_t height = graph.size(), width = graph[0].size();

    std::vector<Cost> potency_u(height, 0), potency_v(width, 0);
    std::vector<size_t> markIndices(width, NOP);

    for (size_t i = 0; i < height; i++) {
        std::vector<size_t > links(width, NOP);
        std::vector<Cost> mins(width, MAX<Cost>);
        std::vector<bool> visited(width, false);

        size_t markedI = i, markedJ = NOP, j = NOP;
        while (markedI != NOP) {
            j = NOP;
            for (size_t j1 = 0; j1 < width; j1++) {
                if (not visited[j1]) {
                    if (graph[markedI][j1] != MAX<Cost> and graph[markedI][j1] - potency_u[markedI] - potency_v[j1] < mins[j1]) {
                        mins[j1] = graph[markedI][j1] - potency_u[markedI] - potency_v[j1];
                        links[j1] = markedJ;
                    }
                    if (j == NOP or mins[j1] < mins[j]) {
                        j = j1;
                    }
                }
            }

            Cost delta = mins[j];
            for (size_t j1 = 0; j1 < width; j1++) {
                if (visited[j1]) {
                    potency_u[markIndices[j1]] += delta;
                    potency_v[j1] -= delta;
                } else {
                    mins[j1] -= delta;
                }
            }
            potency_u[i] += delta;

            visited[j] = true;
            markedJ = j;
            markedI = markIndices[j];
        }

        for (; links[j] != NOP; j = links[j]) {
            markIndices[j] = markIndices[links[j]];
        }
        markIndices[j] = i;
    }

    std::uint64_t value = 0;
    for (size_t j = 0; j < width; j++) {
        if (markIndices[j] != NOP) {
            value += std::uint64_t(graph[markIndices[j]][j]);
        }
    }
    return value;
}



struct ProblemInfo {
    using DPKey = size_t;

    ProblemInfo(size_t n, size_t k, unsigned p)
            : n(n), colors_count(k), penalty(p), costs(k, std::vector<unsigned>(n, 0))
              , mem_min_dp(n * (k + 2), NOP)
              , mem_opt1(n * (k + 2), NOP)
              , mem_nw(n * (k + 2)) {
        init_mem_dp();
    }

    void init_mem_dp() {
        size_t size = n * (colors_count + 2) * (colors_count + 2);
        mem_dp.resize(size, NOP);
    }

    unsigned get_min_dp(VID v, Color p_color) {
        size_t key = v * (colors_count + 2) + p_color;
        unsigned &answer = mem_min_dp[key];
        if (answer == NOP) {
            answer = get_dp(v, 0, p_color);
            for (Color color = 1; color < colors_count; ++color) {
                answer = std::min(answer, get_dp(v, color, p_color));
            }
        }
        return answer;
    }

    unsigned count_opt1(VID v, Color color) {
        size_t key = v * (colors_count + 2) + color;
        unsigned &opt1 = mem_opt1[key];
        if (opt1 == NOP) {
            opt1 = penalty + costs[color][v];
            for (VID child: children[v]) {
                opt1 += get_min_dp(child, color);
            }
        }
        return opt1;
    }

    unsigned get_dp(VID v, Color color, Color p_color) {
        assert(root != NOP);

        DPKey key = combine(v, color, p_color);
        if (mem_dp[key] != NOP) {
            return mem_dp[key];
        }

        unsigned opt1 = count_opt1(v, color);

        auto neighbours_count = children[v].size() + (v != root);
        if (neighbours_count > colors_count) {
            return mem_dp[key] = opt1;
        }

        unsigned opt2 = costs[color][v];
        Graph graph(neighbours_count, std::vector<Cost>(colors_count, MAX<Cost>));
        for (size_t child_no = 0; child_no < children[v].size(); ++child_no) {
            for (Color ch_color = 0; ch_color < colors_count; ++ch_color) {
                Cost cost = get_dp(children[v][child_no], ch_color, color);
                graph[child_no][ch_color] = cost;
            }
        }
        if (v != root) {
            graph[neighbours_count - 1][p_color] = 0;
        }
        opt2 += hungarian(graph);

        return mem_dp[key] = std::min(opt1, opt2);
    }

    DPKey combine(VID v, Color color, Color p_color) {
        DPKey key = v;
        key = key * (colors_count + 2) + color;
        key = key * (colors_count + 2) + p_color;
        return key;
    }

    size_t n, colors_count;
    unsigned penalty;
    std::vector<std::vector<unsigned>> costs;
    std::vector<std::vector<VID>> children;
    std::vector<unsigned> mem_dp;
    std::vector<unsigned> mem_min_dp;
    std::vector<unsigned> mem_opt1;
    std::vector<std::optional<std::pair<Network, Flow>>> mem_nw;
    VID root = NOP;
};

void make_rooted(std::vector<std::vector<VID>> &neighbours, VID v, VID parent) {
    size_t parent_pos = NOP;
    for (size_t i = 0; i < neighbours[v].size(); ++i) {
        VID u = neighbours[v][i];
        if (u == parent) {
            parent_pos = i;
        } else {
            make_rooted(neighbours, u, v);
        }
    }
    if (parent_pos != NOP) {
        neighbours[v].erase(neighbours[v].begin() + long(parent_pos));
    }
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, k;
    unsigned p;
    in >> n >> k >> p;
    ProblemInfo info(n, k, p);
    for (VID v = 0; v < n; ++v) {
        for (Color color = 0; color < k; ++color) {
            in >> info.costs[color][v];
        }
    }
    std::vector<std::vector<VID>> neighbours(n);
    for (size_t i = 0; i < n - 1; ++i) {
        VID u, v;
        in >> u >> v;
        --u, --v;
        neighbours[u].emplace_back(v);
        neighbours[v].emplace_back(u);
    }
    make_rooted(neighbours, 0, NOP);
    info.children = neighbours;
    info.root = 0;

    unsigned answer = info.get_min_dp(info.root, 1);
    out << answer << std::endl;
}

int main() { solve(std::cin, std::cout); }

