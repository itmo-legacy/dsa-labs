#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
3
abacaba
mycabarchive
acabistrue
)";
    auto output = R"(
cab
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
3
a
b
c
)";
    auto output = R"(

)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
2
aba
abafaba
)";
    auto output = R"(
aba
)";
    interact(input, output);
}

TEST(correctness, trivial3) {
    auto input  = R"(
2
abad
abaadabadbabad
)";
    auto output = R"(
abad
)";
    interact(input, output);
}

TEST(correctness, trivial4) {
    auto input  = R"(
1
abad
)";
    auto output = R"(
abad
)";
    interact(input, output);
}


int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
