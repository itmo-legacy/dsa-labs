#ifdef LOCAL
#include "playground.hpp"
#endif

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

std::vector<unsigned> compute_prefix_array(std::string const &s) {
    std::vector<unsigned> prefix(s.length() + 1, 0);
    for (size_t i = 1; i < prefix.size(); ++i) {
        prefix[i] = prefix[i - 1];
        while (prefix[i] > 0 and s[prefix[i]] != s[i]) {
            prefix[i] = prefix[prefix[i] - 1];
        }
        if (s[prefix[i]] == s[i]) {
            ++prefix[i];
        }
    }
    return prefix;
}

void solve(std::istream &in, std::ostream &out) {
    std::string t, s;
    in >> t >> s;
    std::string joint = t + '\0' + s;
    std::cerr << joint << std::endl;
    auto prefix_array = compute_prefix_array(joint);
    std::vector<size_t> entrances;
    for (size_t i = 0; i < prefix_array.size(); ++i) {
        if (prefix_array[i] == t.size()) {
            entrances.emplace_back(i - 2 * t.size() + 1);
        }
    }
    out << entrances.size() << std::endl;
    for (auto &&entrance : entrances) {
        out << entrance << " ";
    }
    out << std::endl;
}

int main() {
    solve(std::cin, std::cout);
}
