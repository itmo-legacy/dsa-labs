#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
bababb
zabacabba
)";
    auto output = R"(
aba
)";
    interact(input, output);
}

TEST(correctness, random) {
    auto input  = R"(
abacaba
mycabarchive
)";
    auto output = R"(
caba
)";
    interact(input, output);
}

TEST(correctness, trivial1) {
    auto input  = R"(
a
b
)";
    auto output = R"(

)";
    interact(input, output);
}

TEST(correctness, trivial2) {
    auto input  = R"(
aba
abafaba
)";
    auto output = R"(
aba
)";
    interact(input, output);
}

TEST(correctness, trivial3) {
    auto input  = R"(
abad
abaadabadbabad
)";
    auto output = R"(
abad
)";
    interact(input, output);
}

TEST(correctness, order) {
    auto input  = R"(
ba
ab
)";
    auto output = R"(
a
)";
    interact(input, output);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
