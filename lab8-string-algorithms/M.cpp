#ifdef LOCAL
#include "playground.hpp"
#endif

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <set>
#include <sstream>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

const std::string taskname = "common";

template <size_t N, auto const &NUMBER>
constexpr auto compute_degrees() {
    std::array<std::decay_t<decltype(NUMBER)>, N + 1> result = {1};
    for (std::size_t i = 1; i < N + 1; ++i) {
        result[i] = result[i - 1] * NUMBER;
    }
    return result;
}

struct Hashes {
    using Number                  = std::uint64_t;
    constexpr static Number STEP  = 41;
    constexpr static auto DEGREES = compute_degrees<200'000, STEP>();

    explicit Hashes(std::string const &s) {
        prefix_hashes.resize(s.length() + 1, 0);
        for (size_t i = 1; i < prefix_hashes.size(); ++i) {
            prefix_hashes[i] = prefix_hashes[i - 1] * STEP + (s[i - 1] - 'a' + 1);
        }
    }

    Number get(std::size_t l, std::size_t r) {
        return prefix_hashes[r] - prefix_hashes[l] * DEGREES[r - l];
    }

    std::vector<Number> prefix_hashes;
};

void solve(std::istream &in, std::ostream &out) {
    std::size_t k = 2;
    std::vector<std::string> strings(k);
    for (size_t i = 0; i < k; ++i) {
        in >> strings[i];
    }
    std::sort(WHOLE(strings), [](auto const &s1, auto const &s2) { return s1.size() < s2.size(); });

    std::vector<Hashes> hashes;
    for (auto &&s : strings) {
        hashes.emplace_back(s);
    }

    std::size_t l = 0, r = strings.front().length() + 1;
    while (r - l > 1) {
        std::size_t length = (r + l) >> 1;
        std::unordered_map<Hashes::Number, unsigned> quantity;
        for (size_t i = 0; i < k; ++i) {
            auto &&s = strings[i];
            auto &&h = hashes[i];
            std::unordered_set<Hashes::Number> encountered;
            for (std::size_t first = 0; first < s.length() - length + 1; ++first) {
                encountered.emplace(h.get(first, first + length));
            }
            for (auto &&hash : encountered) {
                ++quantity[hash];
            }
        }
        if (std::any_of(WHOLE(quantity), [&](auto &&kv) { return kv.second == k; })) {
            l = length;
        } else {
            r = length;
        }
    }
    std::size_t length = l;
    std::unordered_map<Hashes::Number, unsigned> quantity;
    for (size_t i = 0; i < k; ++i) {
        auto &&s = strings[i];
        auto &&h = hashes[i];
        std::unordered_set<Hashes::Number> encountered;
        for (std::size_t first = 0; first < s.length() - length + 1; ++first) {
            encountered.emplace(h.get(first, first + length));
        }
        for (auto &&hash : encountered) {
            ++quantity[hash];
        }
    }
    std::unordered_set<Hashes::Number> proper_hashes;
    for (auto [hash, v] : quantity) {
        if (v == k) {
            proper_hashes.emplace(hash);
        }
    }
    std::set<std::string> proper_substrings;
    auto &&s = strings.front();
    auto &&h = hashes.front();
    for (std::size_t first = 0; first < s.length() - length + 1; ++first) {
        if (proper_hashes.count(h.get(first, first + length))) {
            proper_substrings.emplace(s.substr(first, length));
        }
    }
    out << *proper_substrings.begin() << std::endl;
}

int main() {
    std::ifstream in(taskname + ".in");
    std::ofstream out(taskname + ".out");
    solve(in, out);
    // solve(std::cin, std::cout);
}
