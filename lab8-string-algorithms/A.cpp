// hashes

#ifdef LOCAL
#include "playground.h"
#endif

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

template <std::uint64_t MODULO>
struct Modular {
    static_assert((MODULO * MODULO / MODULO == MODULO), "MODULO should be less than square root of maximal number");

    constexpr Modular() = default;

    template <typename Integer>
    constexpr Modular(Integer data) : data(std::uint64_t(data) % MODULO) {}

    constexpr Modular operator+(Modular other) const {
        return (data + other.data) % MODULO;
    }

    constexpr Modular operator-(Modular other) const {
        return (data - other.data + MODULO) % MODULO;
    }

    constexpr Modular operator*(Modular other) const {
        return (data * other.data) % MODULO;
    }

    constexpr Modular operator/(Modular other) const {
        return *this * other.inverse();
    }

    constexpr Modular inverse() const {
        // n*s + a*t = 1
        // a*t = 1 (mod n)
        std::uint64_t t = 1, r = MODULO,
                      newt = 1, newr = data;
        while (newr != 0) {
            std::uint64_t quotient = r / newr;
            std::tie(t, newt)      = std::make_tuple(newt, t - quotient * newt);
            std::tie(r, newr)      = std::make_tuple(newr, r - quotient * newr);
        }
        if (t < 0) {
            t += MODULO;
        }
        return t;
    }

    constexpr explicit operator std::uint64_t() const {
        return data;
    }

    constexpr bool operator==(Modular other) const {
        return data == other.data;
    }
    std::uint64_t data;
};

template <size_t N, auto const &NUMBER>
constexpr auto compute_degrees() {
    std::array<std::decay_t<decltype(NUMBER)>, N + 1> result = {1};
    for (std::size_t i = 1; i < N + 1; ++i) {
        result[i] = result[i - 1] * NUMBER;
    }
    return result;
}

struct Hashes {
    using Number                  = Modular<4202273773>;
    constexpr static Number STEP  = 41;
    constexpr static auto DEGREES = compute_degrees<200'000, STEP>();

    explicit Hashes(std::string const &s) {
        prefix_hashes.resize(s.length() + 1, 0);
        for (size_t i = 1; i < prefix_hashes.size(); ++i) {
            prefix_hashes[i] = prefix_hashes[i - 1] * STEP + (s[i - 1] - 'a' + 1);
        }
    }

    Number get(std::size_t l, std::size_t r) {
        return prefix_hashes[r] - prefix_hashes[l] * DEGREES[r - l];
    }

    std::vector<Number> prefix_hashes;
};

void solve(std::istream &in, std::ostream &out) {
    std::string s;
    in >> s;
    Hashes hashes(s);
    size_t m;
    in >> m;
    while (m--) {
        size_t l1, r1, l2, r2;
        in >> l1 >> r1 >> l2 >> r2;
        out << (hashes.get(l1 - 1, r1) == hashes.get(l2 - 1, r2) ? "Yes" : "No") << std::endl;
    }
}

int main() {
    solve(std::cin, std::cout);
}
