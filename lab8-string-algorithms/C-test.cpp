#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
aaaAAA
)";
    auto output = R"(
2 1 0 0 0
)";
    interact(input, output);
}

TEST(correctness, example2) {
    auto input  = R"(
abacaba
)";
    auto output = R"(
0 1 0 3 0 1
)";
    interact(input, output);
}


int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
