#include <sstream>

#include "playground.h"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example) {
    auto input  = R"(
trololo
3
1 7 1 7
3 5 5 7
1 1 1 5
)";
    auto output = R"(
Yes
Yes
No
)";
    interact(input, output);
}

TEST(correctness, random) {
    auto input  = R"(
trololo
5
1 7 1 7
3 5 5 7
1 1 1 5
3 3 5 5
4 5 6 7
)";
    auto output = R"(
Yes
Yes
No
Yes
Yes
)";
    interact(input, output);
}

TEST(correctness, curious) {
    auto input  = R"(
aaaaab
4
3 3 1 2
1 1 2 2
4 4 1 1
4 4 6 6
)";
    auto output = R"(
No
Yes
Yes
No
)";
    interact(input, output);
}



int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
