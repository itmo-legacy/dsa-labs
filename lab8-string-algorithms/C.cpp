#ifdef LOCAL
#include "playground.hpp"
#endif

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <set>
#include <sstream>
#include <tuple>
#include <vector>

#define WHOLE(container) std::begin(container), std::end(container)

using std::size_t;

std::vector<unsigned> compute_z_array(std::string const &s) {
    std::vector<unsigned> z(s.length(), 0);
    size_t l = 0, r = 0;
    for (size_t i = 1; i < z.size(); ++i) {
        r = std::max(r, i);
        if (z[i - l] < r - i) {
            z[i] = z[i - l];
        } else {
            l = i, z[i] = unsigned(r - i);
            while (z[i] + i < z.size() and s[z[i] + i] == s[z[i]]) {
                ++z[i];
            }
            r = l + z[i];
        }
    }
    return z;
}

void solve(std::istream &in, std::ostream &out) {
    std::string s;
    in >> s;
    std::vector<unsigned> z_array = compute_z_array(s);
    z_array.erase(z_array.begin());
    for (auto &&el : z_array) {
        out << el << " ";
    }
    out << std::endl;
}

int main() {
    solve(std::cin, std::cout);
}
