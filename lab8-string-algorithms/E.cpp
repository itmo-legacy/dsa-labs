#define WHOLE(container) container.begin(), container.end()

#include <algorithm>
#include <ctime>
#include <unordered_set>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <set>

using namespace std;

typedef unsigned int uint;
typedef signed long long int ll;
typedef unsigned long long int ull;
typedef double lf;

const int INF = (int) 1e9;
const lf EPS = 1e-9;
const ll MOD = (ll) 1e9 + 123;

vector<int>
compute_z_func(string s) {
    const size_t SIZE = s.size();
    vector<int> res(SIZE, 0);
    int left = 0, right = 0;
    for (int i = 1; i < SIZE; ++i) {
        if (res[i - left] < right - i) {
            res[i] = res[i - left];
        }
        else {
            res[i] = max(0, right - i);
            while (res[i] + i < SIZE &&
                    s[res[i] + i] == s[res[i]]) {
                ++res[i];
            }
            right = i + res[i];
            left = i;
        }
    }
    return res;
}

void
test0() {
    string s;
    cin >> s;
    vector<int> z_func = compute_z_func(s);
    const size_t SIZE = s.size();
    vector<int> nums;
    int res = SIZE;
    for (int i = 1; i < SIZE; ++i) {
        if (z_func[i] == SIZE - i &&
                z_func[i] % (SIZE - z_func[i]) == 0) {
            res = i;
            break;
        }
    }
    cout << res << endl;
}

int
main() {
    clock_t start = clock();

    test0();

    double time = (clock() - start) / (double) CLOCKS_PER_SEC * 1000;
    fprintf(stderr, "Time: %f ms\n", time);
    return 0;
}
