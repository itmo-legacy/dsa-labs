#include <sstream>

#include "playground.hpp"
#include <boost/algorithm/string.hpp>
#include <gtest/gtest.h>

void
interact(std::string const &input, std::string const &expected) {
    std::stringstream in;
    std::stringstream out;
    in.str(input);
    solve(in, out);
    std::string actual = out.str();
    EXPECT_EQ(boost::trim_copy(expected), boost::trim_copy(actual));
}

TEST(correctness, example1) {
    auto input  = R"(
4
60
14
3
55
)";
    auto output = R"(
NO
NO
YES
NO
)";
    interact(input, output);
}

TEST(correctness, check2isprime) {
    auto input  = R"(
1
2
)";
    auto output = R"(
YES
)";
    interact(input, output);
}

int
main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
