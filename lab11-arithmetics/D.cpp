#include <array>
#include <cmath>
#include <iostream>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

void
solve(std::istream &in, std::ostream &out) {
    unsigned a, b, n, m;
    in >> a >> b >> n >> m;
    unsigned long long x = a;
    for (size_t i = 0; i < m; ++i) {
        if (x % m == b) {
            break;
        }
        x += n;
    }
    out << x << std::endl;
}

int
main() {
    static constexpr size_t BUF_SIZE = 16777216;
    char cinbuf[BUF_SIZE];
    char coutbuf[BUF_SIZE];
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cin.rdbuf()->pubsetbuf(cinbuf, sizeof(cinbuf));
    std::cout.rdbuf()->pubsetbuf(coutbuf, sizeof(coutbuf));
    solve(std::cin, std::cout);
}
