#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <tuple>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

struct Modular {
    std::uint64_t modulo;
    std::uint64_t data = 0;

    explicit Modular(std::uint64_t modulo) : modulo(modulo) {}

    template<typename Integer>
    Modular(std::uint64_t modulo, Integer data) : modulo(modulo) {
        if (data < 0) {
            std::int64_t remainder = data % std::int64_t(modulo);
            remainder += modulo;
            this->data = std::uint64_t(remainder) % modulo;
        } else {
            this->data = std::uint64_t(data) % modulo;
        }
    }

    Modular
    operator-() const {
        return Modular(modulo, 0) - *this;
    }

    friend Modular
    operator+(Modular a, Modular const &b) {
        return a += b;
    }

    friend Modular
    operator-(Modular a, Modular const &b) {
        return a -= b;
    }

    friend Modular operator*(Modular a, std::uint64_t b) {
        Modular result(a.modulo, 0);
        while (b != 0) {
            if (b % 2 == 1) {
                result += a;
            }
            a += a;
            b /= 2;
        }
        return result;
    }

    friend Modular operator*(Modular a, Modular const &b) {
        return a * b.data;
    }

    friend Modular
    pow(Modular a, unsigned long long n) {
        Modular result(a.modulo, 1);
        while (n != 0) {
            if (n % 2 == 1) {
                result *= a;
            }
            a *= a;
            n /= 2;
        }
        return result;
    }

    Modular &
    operator+=(Modular const &other) {
        data = (data + other.data) % modulo;
        return *this;
    }

    Modular &
    operator+=(std::uint64_t other) {
        data = (data + other) % modulo;
        return *this;
    }

    Modular &
    operator-=(Modular const &other) {
        return *this += -other;
    }

    Modular &
    operator-=(std::uint64_t other) {
        return *this += -other;
    }

    Modular &
    operator*=(std::uint64_t b) {
        return *this = *this * b;
    }

    Modular &
    operator*=(Modular const &other) {
        return *this = *this * other;
    }

    explicit operator std::uint64_t() const {
        return data;
    }

    bool
    operator==(Modular const &other) const {
        return data == other.data;
    }

    bool
    operator==(std::uint64_t other) const {
        return other % modulo == data;
    }

    bool
    operator!=(Modular const &other) const {
        return data != other.data;
    }
};

bool
is_prime(unsigned long long n) {
    if (n == 1) {
        return false;
    } else if (n == 2) {
        return true;
    }

    // 2^s * t = n - 1, where t is odd
    unsigned s = 0;
    unsigned long long t = n - 1;
    while (t % 2 == 0) {
        ++s;
        t /= 2;
    }

    static unsigned constexpr K = 5;
    for (unsigned attempt_no = 0; attempt_no < K; ++attempt_no) {
        Modular a = Modular(n, unsigned(rand()));
        if (a == 0) {
            a += 1;
        }
        if (a == n - 1) {
            a -= 1;
        }
        a = pow(a, t);
        // if n is prime, there is a, such that a^t = 1 (mod n)
        // or exists r < s, such that a^(2rt) = -1 (mod n)
        if (a == 1 or a == n - 1) {
            continue;
        }
        bool second_check = false;
        for (unsigned i = 0; i < s; ++i) {
            a = a * a;
            if (a == 1) {
                return false;
            }
            if (a == n - 1) {
                second_check = true;
                break;
            }
        }
        if (not second_check) {
            return false;
        }
    }
    return true;
}

void
solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    while (n-- > 0) {
        unsigned long long a;
        in >> a;
        out << (is_prime(a) ? "YES\n" : "NO\n");
    }
    out << std::endl;
}

int
main() { solve(std::cin, std::cout); }
