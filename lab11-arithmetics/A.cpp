#include <array>
#include <cmath>
#include <iostream>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

static constexpr size_t LIMIT  = size_t(2e7 + 100) / 2;
static const size_t LIMIT_SQRT = size_t(std::sqrt(LIMIT));

struct Sieve {
    std::array<bool, LIMIT> odd_is_compound{};

    Sieve() {
        size_t i = 0;
        for (i = 1; i < LIMIT_SQRT; ++i) {
            if (not odd_is_compound[i]) {
                size_t num  = i * 2 + 1;
                size_t step = num * 2;
                for (size_t multiple = num * num; multiple < 2 * LIMIT; multiple += step) {
                    odd_is_compound[multiple / 2] = true;
                }
            }
        }
    }
};

void
solve(std::istream &in, std::ostream &out) {
    static const Sieve sieve;
    static auto const &odd_is_compound = sieve.odd_is_compound;

    size_t n;
    in >> n;
    while (n-- > 0) {
        unsigned a;
        in >> a;
        if ((a != 2 and a % 2 == 0) or odd_is_compound[a / 2]) {
            out << "NO\n";
        } else {
            out << "YES\n";
        }
    }
    out << std::endl;
}

int
main() {
    static constexpr size_t BUF_SIZE = 16777216;
    char cinbuf[BUF_SIZE];
    char coutbuf[BUF_SIZE];
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cin.rdbuf()->pubsetbuf(cinbuf, sizeof(cinbuf));
    std::cout.rdbuf()->pubsetbuf(coutbuf, sizeof(coutbuf));
    solve(std::cin, std::cout);
}
