main :: IO ()
main = do
  a <- read <$> getLine
  b <- read <$> getLine
  print (a * b :: Integer)
