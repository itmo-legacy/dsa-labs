#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <iterator>
#include <vector>

#define WHOLE(container) begin(container), end(container)

using std::size_t;

static constexpr unsigned LIMIT     = unsigned(1e6 + 69) / 2;
static const unsigned LIMIT_SQRT    = unsigned(std::sqrt(LIMIT));
static constexpr size_t PRIMES_SIZE = 78502;

std::array<unsigned, PRIMES_SIZE>
extract_primes() {
    std::array<bool, LIMIT> odd_is_compound{};
    for (unsigned i = 1; i < LIMIT_SQRT; ++i) {
        if (not odd_is_compound[i]) {
            size_t num  = i * 2 + 1;
            size_t step = num * 2;
            for (size_t multiple = num * num; multiple < 2 * LIMIT; multiple += step) {
                odd_is_compound[multiple / 2] = true;
            }
        }
    }
    std::array<unsigned, PRIMES_SIZE> primes = {2};
    size_t pos                               = 0;
    for (unsigned i = 1; i < LIMIT; ++i) {
        if (not odd_is_compound[i]) {
            primes[++pos] = i * 2 + 1;
        }
    }
    return primes;
}

std::array<unsigned, PRIMES_SIZE> primes = extract_primes();

void
solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    while (n-- > 0) {
        unsigned a;
        in >> a;
        std::vector<unsigned> components;
        auto prime_it   = primes.begin();
        unsigned sqrt_a = unsigned(std::sqrt(a));
        while (*prime_it <= sqrt_a) {
            auto prime = *prime_it;
            while (a % prime == 0) {
                a /= prime;
                components.emplace_back(prime);
            }
            ++prime_it;
        }
        if (a != 1) {
            components.emplace_back(a);
        }
        std::copy(WHOLE(components), std::ostream_iterator<unsigned>(out, " "));
        out << "\n";
    }
    out << std::endl;
}

int
main() {
    static constexpr size_t BUF_SIZE = 16777216;
    char cinbuf[BUF_SIZE];
    char coutbuf[BUF_SIZE];
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cin.rdbuf()->pubsetbuf(cinbuf, sizeof(cinbuf));
    std::cout.rdbuf()->pubsetbuf(coutbuf, sizeof(coutbuf));
    solve(std::cin, std::cout);
}
