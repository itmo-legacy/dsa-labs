#include <unordered_set>
#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

const std::string taskname = "mail";

using std::size_t;
using std::vector;

constexpr size_t N = 1004010;
constexpr size_t LOG = 21;

struct Node {
    size_t depth = 0;
    Node *parents[LOG];
    vector<Node *> children;
    int colors_count = 1;
    int color;
};

size_t n;
Node nodes[N];
Node *last_color[N];

void preprocess() {
    for (size_t i = 1; i < LOG; ++i) {
        for (size_t v = 1; v < n + 1; ++v) {
            nodes[v].parents[i] = nodes[v].parents[i - 1]->parents[i - 1];
        }
    }
}

Node *find_lca(Node *u, Node *v) {
    if (u->depth > v->depth) { std::swap(v, u); }
    size_t diff = v->depth - u->depth;
    for (int i = 0; i < LOG; ++i) {
        if ((diff >> i) & 1U) { v = v->parents[i]; }
    }
    if (u == v) { return u; }
    for (int maxd = LOG; maxd-- > 0;) {
        if (u->parents[maxd] != v->parents[maxd]) {
            u = u->parents[maxd], v = v->parents[maxd];
        }
    }
    return u->parents[0];
}

void dfs(Node *node, size_t depth = 0) {
    node->depth = depth;
    if (last_color[node->color]) {
        --find_lca(last_color[node->color], node)->colors_count;
    }
    last_color[node->color] = node;
    node->colors_count = 1;
    for (auto &&child: node->children) { dfs(child, depth + 1); }
    for (auto &&child: node->children) { node->colors_count += child->colors_count; }
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    for (int i = 1; i < n + 1; ++i) {
        int p, c;
        in >> p >> c;
        nodes[i].parents[0] = &nodes[p];
        nodes[i].color = c;
        nodes[p].children.push_back(&nodes[i]);
    }
    Node *root = nodes[0].children[0];
    root->parents[0] = root;
    preprocess();

    dfs(root);
    for (int i = 1; i < n + 1; ++i) {
        out << nodes[i].colors_count << " ";
    }
    out << "\n";
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}