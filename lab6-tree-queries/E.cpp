#include <unordered_set>
#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

const std::string taskname = "mail";

using std::size_t;
using std::vector;

using val_t = int;

struct Node {
    size_t index;
    size_t subtree_size;
    vector<Node *> children{};
    Node *parent = nullptr;
};
size_t n;
std::vector<Node> nodes;
std::vector<Node> decomposed_nodes;

std::unordered_set<int> roots;

size_t count_sizes(Node *v, Node *pr) {
    v->subtree_size = 1;
    for (auto &&child: v->children) {
        if (not roots.count(child->index) and child != pr) { v->subtree_size += count_sizes(child, v); }
    }
    return v->subtree_size;
}

Node &to_decomposed_tree(Node *v, Node *root, size_t size);

Node &descend(Node *v, Node *pr, Node *root, size_t subtree_size) {
    size_t maxs = 0;
    Node *maxch = nullptr;
    for (auto &&child: v->children) {
        if (not roots.count(child->index) and pr != child and child->subtree_size > maxs) {
            maxs = child->subtree_size;
            maxch = child;
        }
    }
    if (2 * maxs <= subtree_size) {
        Node &result = decomposed_nodes[v->index];
        roots.insert(v->index);
        result.parent = root;
        for (auto &&child: v->children) {
            if (not roots.count(child->index)) {
                if (child != pr) {
                    result.children.push_back(&to_decomposed_tree(child, v, child->subtree_size));
                } else {
                    result.children.push_back(&to_decomposed_tree(child, v, size_t(-1)));
                }
            }
        }
        return result;
    } else {
        return descend(maxch, v, root, subtree_size);
    }
}


Node &to_decomposed_tree(Node *v, Node *root, size_t size) {
    if (size == -1) {
        count_sizes(v, v);
    }
    return descend(v, root, root, v->subtree_size);
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    nodes.resize(n + 1);
    decomposed_nodes.resize(n + 1);
    for (size_t i = 0; i < n + 1; ++i) {
        nodes[i].index = i;
        decomposed_nodes[i].index = i;
    }
    for (size_t i = 0; i < n - 1; ++i) {
        int v, u;
        in >> v >> u;
        nodes[u].children.push_back(&nodes[v]);
        nodes[v].children.push_back(&nodes[u]);
    }
    to_decomposed_tree(&nodes[1], &nodes[0], -1);
    for (int i = 1; i < n + 1; ++i) {
        out << decomposed_nodes[i].parent->index << " ";
    }
    out << "\n";
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}