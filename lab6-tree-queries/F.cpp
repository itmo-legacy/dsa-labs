#include <unordered_set>
#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

const std::string taskname = "mail";

using std::size_t;
using std::vector;

constexpr size_t N = 200'100;
constexpr size_t LOG = 19;

struct Node {
    size_t depth = 0;
    Node *parents[LOG];
    bool deleted = false;
    Node *existing_parent = nullptr;
};

Node nodes[N];

Node *existing_parent(Node *node) {
    if (not node->existing_parent->deleted) { return node->existing_parent; }
    return node->existing_parent = existing_parent(node->existing_parent->parents[0]);
}

void add(Node *u, Node *ancestor) {
    u->parents[0] = ancestor;
    u->existing_parent = u;
    u->depth = ancestor->depth + 1; // or existing parent depth
    for (int i = 1; i < LOG; ++i) { u->parents[i] = u->parents[i - 1]->parents[i - 1]; }
}

Node *find_lca(Node *u, Node *v) {
    if (u->depth > v->depth) { std::swap(v, u); }
    size_t diff = v->depth - u->depth;
    for (int i = 0; i < LOG; ++i) {
        if ((diff >> i) & 1U) { v = v->parents[i]; }
    }
    if (u == v) { return u; }
    for (int maxd = LOG; maxd-- > 0;) {
        if (u->parents[maxd] != v->parents[maxd]) {
            u = u->parents[maxd], v = v->parents[maxd];
        }
    }
    return existing_parent(u->parents[0]);
}

void solve(std::istream &in, std::ostream &out) {
    size_t m;
    in >> m;
    nodes[1].existing_parent = &nodes[1];
    add(&nodes[1], &nodes[1]);
    unsigned count = 1;
    while (m--) {
        std::string instruction;
        in >> instruction;
        if (instruction == "+") {
            int u;
            in >> u;
            add(&nodes[++count], &nodes[u]);
        } else if (instruction == "-") {
            int u;
            in >> u;
            nodes[u].deleted = true;
            nodes[u].existing_parent = existing_parent(nodes[u].parents[0]);
        } else {
            int u, v;
            in >> u >> v;
            out << find_lca(&nodes[u], &nodes[v]) - &nodes[0] << "\n";
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}