#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

using std::vector;

constexpr size_t N = size_t(5e5 + 10);
constexpr size_t LOG = 22;

struct Tree;

struct Node {
    size_t depth = 0;
    size_t subtree_size = 1;
    Node *parents[LOG];
    vector<Node *> children;
    Tree *tree = nullptr;
    size_t pos = 0;
};

struct Tree {
    struct TNode {
        long long value;
        size_t lb, rb;
        TNode *l, *r;
        long long adjustment = 0;

        TNode(long long value, size_t lb, size_t rb, TNode *l = nullptr, TNode *r = nullptr)
                : value(value), lb(lb), rb(rb), l(l), r(r) {}

        size_t length() { return rb - lb; }

        bool is_leaf() { return length() == 1; }

        long long adjusted() { return value + (long long) length() * adjustment; }

        long long query(size_t pos) {
            push();
            if (pos < lb or rb <= pos) { return 0; }
            else if (is_leaf()) { return adjusted(); }
            else { return l->query(pos) + r->query(pos); }
        }

        void bulk(size_t alb, size_t arb, int d) {
            if (arb <= lb or rb <= alb) { return; }
            if (alb <= lb and rb <= arb) {
                adjustment += d;
            } else {
                l->bulk(alb, arb, d);
                r->bulk(alb, arb, d);
            }
        }

        void push() {
            if (not is_leaf()) {
                l->adjustment += adjustment;
                r->adjustment += adjustment;
                value = adjusted();
                adjustment = 0;
            }
        }
    };

    std::vector<long long> base;
    Node *chain_root = nullptr;

    void add(long long v) { base.push_back(v); }

    void build() { build(root, 0, base.size()); }

    long long query(size_t pos) { return root->query(pos); }

    void bulk(size_t lb, size_t rb, int d) { root->bulk(lb, rb, d); }

    size_t size() { return root->length(); }

private:
    TNode *root;

    void build(TNode *&node, size_t lb, size_t rb) {
        if (rb - lb == 1) {
            node = new TNode(base[lb], lb, rb);
            return;
        }
        TNode *l = nullptr, *r = nullptr;
        size_t mb = lb + rb >> 1U;
        build(l, lb, mb);
        build(r, mb, rb);
        node = new TNode(l->value + r->value, lb, rb, l, r);
    }


};

size_t n;
Node nodes[N];

void hld(Node *v, size_t pos, Tree *tree) {
    if (pos == 0) { tree->chain_root = v; }
    v->pos = pos;
    v->tree = tree;
    tree->add(0);

    auto max_it = std::max_element(v->children.begin(), v->children.end(),
                                   [](Node *const one, Node *const two) {
                                       return one->subtree_size < two->subtree_size;
                                   });
    if (max_it != v->children.end()) { hld(*max_it, pos + 1, tree); }
    else { v->tree->build(); }

    for (auto it = v->children.begin(); it != v->children.end(); ++it) {
        if (it != max_it) {
            hld(*it, 0, new Tree());
        }
    }
}

void to_tree(Node *v, Node *parent, size_t depth) {
    v->parents[0] = parent;
    v->depth = depth;
    auto it = std::find(v->children.begin(), v->children.end(), parent);
    if (it != v->children.end()) { v->children.erase(it); }
    for (auto &&child: v->children) {
        to_tree(child, v, depth + 1);
        v->subtree_size += child->subtree_size;
    }
}

void preprocess() {
    for (size_t i = 1; i < LOG; ++i) {
        for (size_t v = 1; v < n + 1; ++v) {
            nodes[v].parents[i] = nodes[v].parents[i - 1]->parents[i - 1];
        }
    }
}

Node *find_lca(Node *u, Node *v) {
    if (u->depth > v->depth) { std::swap(v, u); }
    size_t diff = v->depth - u->depth;
    for (int i = 0; i < LOG; ++i) {
        if ((diff >> i) & 1U) { v = v->parents[i]; }
    }
    if (u == v) { return u; }
    for (int maxd = LOG; maxd-- > 0;) {
        if (u->parents[maxd] != v->parents[maxd]) {
            u = u->parents[maxd], v = v->parents[maxd];
        }
    }
    return u->parents[0];
}

void bulk_up(Node *low, Node *high, int d) {
    while (low->tree != high->tree) {
        low->tree->bulk(0, low->pos + 1, d);
        low = low->tree->chain_root->parents[0];
    }
    low->tree->bulk(high->pos, low->pos + 1, d);
}

void bulk(Node *u, Node *v, int d) {
    Node *lca = find_lca(u, v);
    bulk_up(u, lca, d);
    bulk_up(v, lca, d);
    lca->tree->bulk(lca->pos, lca->pos + 1, -d);
}

long long query(Node *u) { return u->tree->query(u->pos); }

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    for (std::size_t i = 0; i < n - 1; ++i) {
        int v, u;
        in >> v >> u;
        nodes[u].children.push_back(&nodes[v]);
        nodes[v].children.push_back(&nodes[u]);
    }
    to_tree(&nodes[1], &nodes[1], 0);
    preprocess();
    hld(&nodes[1], 0, new Tree());
    size_t m;
    in >> m;
    while (m--) {
        std::string type;
        in >> type;
        if (type == "+") {
            int u, v, d;
            in >> u >> v >> d;
            bulk(&nodes[u], &nodes[v], d);
        } else {
            int v;
            in >> v;
            out << query(&nodes[v]) << "\n";
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin("roads.in");
//        std::ofstream fout("roads.out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}