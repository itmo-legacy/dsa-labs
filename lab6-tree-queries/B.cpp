#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <algorithm>
#include <cassert>

const std::string taskname = "minonpath";

constexpr size_t N = size_t(3e5);
constexpr size_t LOG = 20;
size_t n;
int ps[N] = {0, 1};
int ws[N] = {0, int(2e7)};
size_t ds[N];
int jump_vs[N][LOG];
int jump_mins[N][LOG];

void preprocess() {
    for (std::size_t v = 1; v < n + 1; ++v) {
        jump_vs[v][0] = ps[v];
        jump_mins[v][0] = ws[v];
        for (std::size_t i = 1; i < LOG; ++i) {
            jump_vs[v][i] = jump_vs[jump_vs[v][i - 1]][i - 1];
            jump_mins[v][i] = std::min(jump_mins[v][i - 1], jump_mins[jump_vs[v][i - 1]][i - 1]);
        }
    }
}

int find(int u, int v) {
    if (ds[u] > ds[v]) { std::swap(u, v); }
    int result = int(2e7);
    size_t diff = ds[v] - ds[u];
    assert(diff >= 0);
    for (int i = 0; i < LOG; ++i) {
        if (diff & 1U) {
            result = std::min(result, jump_mins[v][i]);
            v = jump_vs[v][i];
        }
        diff >>= 1;
    }
    assert(ds[u] == ds[v]);
    if (u == v) { return result; }
    int maxd = LOG - 1;
    while (ps[u] != ps[v]) {
        while (jump_vs[u][maxd] == jump_vs[v][maxd]) { --maxd; }
        result = std::min({result, jump_mins[u][maxd], jump_mins[v][maxd]});
        u = jump_vs[u][maxd], v = jump_vs[v][maxd];
    }
    assert(ps[u] == ps[v]);
    return std::min({result, ws[u], ws[v]});
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    for (std::size_t i = 0; i < n - 1; ++i) {
        in >> ps[i + 2];
        in >> ws[i + 2];
        ds[i + 2] = ds[ps[i + 2]] + 1;
    }
    preprocess();
    size_t m;
    in >> m;
    while (m--) {
        int u, v;
        in >> u >> v;
        out << find(u, v) << "\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
//        std::cin.tie(nullptr);
//        std::ios::sync_with_stdio(false);
//        solve(std::cin, std::cout);
    }
}