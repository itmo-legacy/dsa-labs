#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

const std::string taskname = "mail";

using std::size_t;
using std::vector;

constexpr size_t LOG = 17;
using val_t = int;
using adj_t = int;

struct Tree;

struct Node {
    val_t value;
    size_t depth = 0;
    size_t subtree_size = 1;
    Node *parents[LOG];
    vector<Node *> children;
    Tree *tree = nullptr;
    size_t pos = 0;
};

template<typename T>
struct Monoid {
    static T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

struct MaxM : public Monoid<val_t> { // CONNECTED WITH Tree::adjusted
    static val_t MEMPTY() { return -val_t(1e8); }

    static val_t MAPPEND(val_t const &one, val_t const &two) { return std::max(one, two); }
};

struct AdjM : public Monoid<adj_t> { // CONNECTED WITH Tree::adjusted
    static adj_t MEMPTY() { return -1; }

    static adj_t MAPPEND(adj_t const &one, adj_t const &two) {
        if (one == MEMPTY()) { return two; }
        else if (two == MEMPTY()) { return one; }
        else { return two; }
    }
};

struct Tree {
    struct TNode {
        val_t value;
        size_t lb, rb;
        TNode *l, *r;
        adj_t adjustment = AdjM::MEMPTY();

        TNode(val_t value, size_t lb, size_t rb, TNode *l = nullptr, TNode *r = nullptr)
                : value(value), lb(lb), rb(rb), l(l), r(r) {}

        size_t length() { return rb - lb; }

        bool is_leaf() { return length() == 1; }

        val_t adjusted() { return AdjM::MAPPEND(value, adjustment); } // CHANGE IF MONOID CHANGES

        val_t query(size_t alb, size_t arb) {
            push();
            if (arb <= lb or rb <= alb) { return MaxM::MEMPTY(); }
            else if (alb <= lb and rb <= arb) { return adjusted(); }
            else { return MaxM::MAPPEND(l->query(alb, arb), r->query(alb, arb)); }
        }

        void bulk(size_t alb, size_t arb, val_t d) {
            if (arb <= lb or rb <= alb) { return; }
            if (alb <= lb and rb <= arb) {
                adjustment = AdjM::MAPPEND(adjustment, d);
            } else {
                l->bulk(alb, arb, d);
                r->bulk(alb, arb, d);
                update();
            }
        }

        void push() {
            if (not is_leaf()) {
                l->adjustment = AdjM::MAPPEND(l->adjustment, adjustment);
                r->adjustment = AdjM::MAPPEND(r->adjustment, adjustment);
                value = adjusted();
                adjustment = AdjM::MEMPTY();
            }
        }

        void update() {
            adjustment = AdjM::MEMPTY();
            value = MaxM::MEMPTY();
            if (not is_leaf()) {
                value = MaxM::MAPPEND(value, l->adjusted());
                value = MaxM::MAPPEND(value, r->adjusted());
            }
        }
    };

    std::vector<val_t> base;
    Node *chain_root = nullptr;

    void add(val_t v) { base.push_back(v); }

    void build() { build(root, 0, base.size()); }

    val_t query(size_t lb, size_t rb) { return root->query(lb, rb); }

    void bulk(size_t lb, size_t rb, val_t d) { root->bulk(lb, rb, d); }

    size_t size() { return root->length(); }

private:
    TNode *root;

    void build(TNode *&node, size_t lb, size_t rb) {
        if (rb - lb == 1) {
            node = new TNode(base[lb], lb, rb);
            return;
        }
        TNode *l = nullptr, *r = nullptr;
        size_t mb = lb + rb >> 1U;
        build(l, lb, mb);
        build(r, mb, rb);
        node = new TNode(MaxM::MAPPEND(l->value, r->value), lb, rb, l, r);
    }
};

size_t n;
std::vector<Node> nodes;

void hld(Node *v, size_t pos, Tree *tree) {
    if (pos == 0) { tree->chain_root = v; }
    v->pos = pos;
    v->tree = tree;
    tree->add(v->value);

    auto max_it = std::max_element(v->children.begin(), v->children.end(),
                                   [](Node *const one, Node *const two) {
                                       return one->subtree_size < two->subtree_size;
                                   });
    if (max_it != v->children.end()) { hld(*max_it, pos + 1, tree); }
    else { v->tree->build(); }

    for (auto it = v->children.begin(); it != v->children.end(); ++it) {
        if (it != max_it) {
            hld(*it, 0, new Tree());
        }
    }
}

void to_tree(Node *v, Node *parent, size_t depth) {
    v->parents[0] = parent;
    v->depth = depth;
    auto it = std::find(v->children.begin(), v->children.end(), parent);
    if (it != v->children.end()) { v->children.erase(it); }
    for (auto &&child: v->children) {
        to_tree(child, v, depth + 1);
        v->subtree_size += child->subtree_size;
    }
}

void preprocess() {
    for (size_t i = 1; i < LOG; ++i) {
        for (size_t v = 1; v < n + 1; ++v) {
            nodes[v].parents[i] = nodes[v].parents[i - 1]->parents[i - 1];
        }
    }
}

Node *find_lca(Node *u, Node *v) {
    if (u->depth > v->depth) { std::swap(v, u); }
    size_t diff = v->depth - u->depth;
    for (int i = 0; i < LOG; ++i) {
        if ((diff >> i) & 1U) { v = v->parents[i]; }
    }
    if (u == v) { return u; }
    for (int maxd = LOG; maxd-- > 0;) {
        if (u->parents[maxd] != v->parents[maxd]) {
            u = u->parents[maxd], v = v->parents[maxd];
        }
    }
    return u->parents[0];
}

void bulk_up(Node *low, Node *high, val_t d) {
    while (low->tree != high->tree) {
        low->tree->bulk(0, low->pos + 1, d);
        low = low->tree->chain_root->parents[0];
    }
    low->tree->bulk(high->pos, low->pos + 1, d);
}

void bulk(Node *u, Node *v, val_t d) {
    Node *lca = find_lca(u, v);
    bulk_up(u, lca, d);
    bulk_up(v, lca, d);
}

val_t query_up(Node *low, Node *high) {
    val_t result = MaxM::MEMPTY();
    while (low->tree != high->tree) {
        result = MaxM::MAPPEND(result, low->tree->query(0, low->pos + 1));
        low = low->tree->chain_root->parents[0];
    }
    return MaxM::MAPPEND(result, low->tree->query(high->pos, low->pos + 1));
}

val_t query(Node *u, Node *v) {
    Node *lca = find_lca(u, v);
    return MaxM::MAPPEND(query_up(u, lca), query_up(v, lca));
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    nodes.resize(n + 1);
    for (size_t i = 0; i < n; ++i) { in >> nodes[i + 1].value; }
    for (size_t i = 0; i < n - 1; ++i) {
        int v, u;
        in >> v >> u;
        nodes[u].children.push_back(&nodes[v]);
        nodes[v].children.push_back(&nodes[u]);
    }
    to_tree(&nodes[1], &nodes[1], 0);
    preprocess();
    hld(&nodes[1], 0, new Tree());
    size_t m;
    in >> m;
    while (m--) {
        std::string type;
        in >> type;
        if (type == "?") {
            int u, v;
            in >> u >> v;
            out << query(&nodes[u], &nodes[v]) << "\n";
        } else {
            int v;
            val_t h;
            in >> v >> h;
            bulk(&nodes[v], &nodes[v], h);
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
//        std::cin.tie(nullptr);
//        std::ios::sync_with_stdio(false);
//        solve(std::cin, std::cout);
    }
}