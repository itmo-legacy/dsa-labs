#include <unordered_set>
#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>
#include <cassert>

using std::size_t;
using std::vector;

constexpr size_t N = 204'010;

struct Node {
    Node *left = nullptr, *right = nullptr;
    Node *parent = nullptr;
    bool revert = false;
    size_t size = 1;

    bool is_root() { return not parent or (parent->left != this and parent->right != this); }

    void push() {
        if (revert) {
            revert = false;
            std::swap(left, right);
            if (left) { left->revert ^= 1; }
            if (right) { right->revert ^= 1; }
        }
    }
};

size_t get_size(Node *node) { return node ? node->size : 0; }

void connect(Node *ch, Node *p, int is_left) {
    if (ch) { ch->parent = p; }
    if (is_left == 1) {
        p->left = ch;
        p->size += get_size(ch);
    } else if (is_left == 0) {
        p->right = ch;
        p->size += get_size(ch);
    }
}

void rotate(Node *x) {
    Node *p = x->parent;
    Node *g = p->parent;
    bool left_child = (x == p->left);
    connect(left_child ? x->right : x->left, p, left_child);
    connect(p, x, not left_child);
    connect(x, g, not p->is_root() ? p == g->left : -1);
}

void splay(Node *x) {
    while (not x->is_root()) {
        Node *p = x->parent;
        Node *g = p->parent;
        if (not p->is_root()) { g->push(); }
        p->push(), x->push();
        if (not p->is_root()) { rotate((x == p->left) == (p == g->left) ? p : x); }
        rotate(x);
    }
    x->push();
    x->update();
}

Node *expose(Node *x) {
    Node *last = nullptr;
    for (Node *y = x; y != nullptr; y = y->parent) {
        splay(y);
        y->size -= get_size(y->left);
        y->left = last;
        y->size += get_size(y->left);
        last = y;
    }
    splay(x);
    return last;
}

void make_root(Node *x) {
    expose(x);
    x->revert ^= 1;
}

bool connected(Node *x, Node *y) {
    if (x == y) { return true; }
    expose(x);
    expose(y);
    return x->parent != nullptr;
}

void link(Node *x, Node *y) {
    assert(not connected(x, y));
    make_root(x);
    x->parent = y;
    y->size += y->size;
    y->left = x;
}

void cut(Node *x, Node *y) {
    make_root(x);
    expose(y);
    y->right->parent = nullptr;
    y->size -= y->right->size;
    y->right = nullptr;
}

size_t n;
Node nodes[N];

void solve(std::istream &in, std::ostream &out) {
    size_t m;
    in >> n >> m;
    while (m--) {
        std::string instruction;
        in >> instruction;
        if (instruction == "link") {
            int u, v;
            in >> u >> v;
            link(&nodes[u], &nodes[v]);
        } else if (instruction == "cut") {
            int u, v;
            in >> u >> v;
            cut(&nodes[u], &nodes[v]);
        } else {
            int u;
            in >> u;
            make_root(&nodes[u]);
            out << nodes[u].size << "\n";
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}