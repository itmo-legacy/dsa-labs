#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <cassert>

constexpr size_t N = size_t(3e5);
constexpr size_t LOG = 20   ;
size_t n;
int ps[N] = {0, 1};
size_t ds[N];
int dp[N][LOG];

void preprocess() {
    for (std::size_t v = 1; v < n + 1; ++v) {
        dp[v][0] = ps[v];
        for (std::size_t i = 1; i < LOG; ++i) {
            dp[v][i] = dp[dp[v][i - 1]][i - 1];
        }
    }
}

int find(int u, int v) {
    if (ds[u] > ds[v]) { std::swap(u, v); }
    size_t diff = ds[v] - ds[u];
    assert(diff >= 0);
    for (int i = 0; i < LOG; ++i) {
        if (diff & 1U) { v = dp[v][i]; }
        diff >>= 1;
    }
    assert(ds[u] == ds[v]);
    if (u == v) { return u; }
    int maxd = LOG - 1;
    while (ps[u] != ps[v]) {
        while (dp[u][maxd] == dp[v][maxd]) { --maxd; }
        u = dp[u][maxd], v = dp[v][maxd];
    }
    assert(ps[u] == ps[v]);
    return ps[u];
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    for (std::size_t i = 0; i < n - 1; ++i) {
        in >> ps[i + 2];
        ds[i + 2] = ds[ps[i + 2]] + 1;
    }
    preprocess();
    size_t m;
    in >> m;
    while (m--) {
        int u, v;
        in >> u >> v;
        out << find(u, v) << "\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin("roads.in");
//        std::ofstream fout("roads.out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}