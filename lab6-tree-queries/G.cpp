#include <unordered_set>
#include <iostream>
#include <cstring>
#include <fstream>
#include <limits>
#include <vector>
#include <algorithm>

const std::string taskname = "mail";

using std::size_t;
using std::vector;

constexpr size_t N = 104'010;
constexpr size_t LOG = 18;

struct Node {
    size_t depth = 0;
    Node *parents[LOG];
    std::vector<Node *> children;
    unsigned time;
};

size_t n;
Node nodes[N];

void preprocess() {
    for (size_t i = 1; i < LOG; ++i) {
        for (size_t v = 1; v < n + 1; ++v) {
            nodes[v].parents[i] = nodes[v].parents[i - 1]->parents[i - 1];
        }
    }
}

void find_times_and_depth(Node *root, unsigned &t, size_t depth = 0) {
    root->time = t++;
    root->depth = depth;
    for (auto &&child: root->children) { find_times_and_depth(child, t, depth + 1); }
}

Node *find_lca(Node *u, Node *v) {
    if (u->depth > v->depth) { std::swap(v, u); }
    size_t diff = v->depth - u->depth;
    for (int i = 0; i < LOG; ++i) {
        if ((diff >> i) & 1U) { v = v->parents[i]; }
    }
    if (u == v) { return u; }
    for (int maxd = LOG; maxd-- > 0;) {
        if (u->parents[maxd] != v->parents[maxd]) {
            u = u->parents[maxd], v = v->parents[maxd];
        }
    }
    return u->parents[0];
}

void solve(std::istream &in, std::ostream &out) {
    in >> n;
    Node *root = nullptr;
    for (int i = 1; i < n + 1; ++i) {
        int p;
        in >> p;
        if (p == -1) {
            p = i;
            root = &nodes[i];
        } else {
            nodes[p].children.push_back(&nodes[i]);
        }
        nodes[i].parents[0] = &nodes[p];
    }
    preprocess();
    unsigned t = 0;
    find_times_and_depth(root, t);
    size_t g;
    in >> g;
    std::vector<Node *> group;
    while (g--) {
        size_t k;
        in >> k;
        group.resize(k + 1);
        group[k] = root;
        while (k--) {
            int u;
            in >> u;
            group[k] = &nodes[u];
        }
        std::sort(group.begin(), group.end(), [](Node const *a, Node const *b) { return a->time < b->time; });
        const size_t size = group.size();
        int result = 0;
        for (size_t i = 1; i < size; ++i) {
            result += group[i]->depth - find_lca(group[i - 1], group[i])->depth;
        }
        out << result + 1 << "\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        std::cin.tie(nullptr);
        std::ios::sync_with_stdio(false);
        solve(std::cin, std::cout);
    }
}