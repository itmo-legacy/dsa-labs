#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <experimental/optional>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <unordered_set>
#include <cstdlib>

using std::size_t;
using std::experimental::optional;

using value_t = int;

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

class Treap {
    struct Config {
        static constexpr size_t BOUND = 1'000'000'000;
        size_t size = 0;
        size_t leftmost_empty_index = BOUND;

        Config() = default;

        explicit Config(value_t value) : size(1), leftmost_empty_index(value == 0 ? 0 : BOUND) {};

        Config(size_t size, size_t leftmost_empty_index) : size(size), leftmost_empty_index(leftmost_empty_index) {}

        bool operator==(Config const &rhs) const {
            return size == rhs.size and leftmost_empty_index == rhs.leftmost_empty_index;
        };
    };

    struct ConfigM : public Monoid<Config> {
        static Config MAPPEND(Config const &one, Config const &two) {
            return {one.size + two.size,
                    std::min(one.leftmost_empty_index, one.size + two.leftmost_empty_index)};
        }
    };

    struct Node {
        value_t value;
        Config config;
        int priority;
        Node *lchild = nullptr, *rchild = nullptr;

        explicit Node(value_t value) : value(value), config(value), priority(std::rand()) {}

        size_t key() { return get_config(lchild).size; }

        Node *set_children(Node *lchild, Node *rchild) {
            this->lchild = lchild;
            this->rchild = rchild;
            update();
            return this;
        }

        void update() {
            config = Config(value);
            config = ConfigM::MAPPEND(get_config(lchild), config);
            config = ConfigM::MAPPEND(config, get_config(rchild));
        }
    };

    static Config
    get_config(Node *node) { return node ? node->config : ConfigM::MEMPTY(); }

    static Node *
    merge(Node *left, Node *right) {
        if (left == nullptr) { return right; }
        if (right == nullptr) { return left; }
        if (left->priority < right->priority) {
            auto right_part = merge(left->rchild, right);
            return left->set_children(left->lchild, right_part);
        } else {
            auto left_part = merge(left, right->lchild);
            return right->set_children(left_part, right->rchild);
        }
    }

    static Node *
    merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

    static std::pair<Node *, Node *>
    split(Node *root, size_t key /* left part size */) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (key <= root->key()) {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->lchild, key);
            root->set_children(right_part, root->rchild);
            return {left_part, root};
        } else {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->rchild, key - root->key() - 1);
            root->set_children(root->lchild, left_part);
            return {root, right_part};
        }
    };

    static std::tuple<Node *, Node *, Node *>
    select(Node *root, size_t key) {
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, key + 1);
        std::tie(left, middle) = split(left, key);
        return {left, middle, right};
    };

    void print(Node *root, std::ostream &out, std::string const &indent) {
        if (root) {
            print(root->rchild, out, indent + "\t");
            out << indent << root->value << ":" << root->config.leftmost_empty_index << "\n";
            print(root->lchild, out, indent + "\t");
        }
    }

    void fill(Node *root, std::vector<int> &array) {
        if (root) {
            fill(root->lchild, array);
            array.emplace_back(root->value);
            fill(root->rchild, array);
        }
    }

private:
    Node *root = nullptr;

public:
    explicit Treap(size_t size) {
        for (std::size_t i = 0; i < size; ++i) {
            root = merge(root, new Node(value_t()));
        }
    }

    size_t size() { return get_config(root).size; }

    void merge(Treap const &another) { this->root = merge(root, another.root); }

    void insert(size_t place, value_t value) {
        Node *left, *right;
        std::tie(left, right) = split(root, place);
        Node *p1, *empty, *p2;
        std::tie(p1, empty, p2) = select(right, get_config(right).leftmost_empty_index);
        root = merge(left, new Node(value), merge(p1, p2));
    }

    void print(std::ostream &out) { print(root, out, ""); }

    std::vector<int> fill() {
        std::vector<int> array;
        array.reserve(size());
        fill(root, array);
        return array;
    }
};

void solve(std::istream &in, std::ostream &out) {
    size_t n, m;
    in >> n >> m;
    Treap treap(m);
    for (size_t i = 0; i < n; ++i) {
        size_t place;
        in >> place;
        treap.insert(--place, int(i + 1));
    }
    std::vector<int> array = treap.fill();
    auto it = std::prev(array.end());
    while (it != array.begin() && *it == 0) {
        --it;
    }
    array.erase(++it, array.end());
    out << array.size() << std::endl;
    for (auto &&item: array) {
        out << item << " ";
    }
    out << std::endl;
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        solve(std::cin, std::cout);
    }
    return 0;
}