#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <experimental/optional>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <unordered_set>
#include <cstdlib>
#include <bitset>

using std::size_t;
using std::experimental::optional;

struct value_t {
    size_t code;
    size_t size;
};
using alphabet_t = std::uint32_t;

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

class Treap {
    struct Config {
        size_t size = 0;
        alphabet_t alphabet = 0;

        Config() = default;

        explicit Config(value_t value) : size(value.size) {
            alphabet = 1U << value.code;
        };

        Config(size_t size, alphabet_t alphabet) : size(size), alphabet(alphabet) {};

    };

    struct ConfigM : public Monoid<Config> {
        static Config MAPPEND(Config const &one, Config const &two) {
            return {one.size + two.size,
                    one.alphabet | two.alphabet};
        }
    };

    struct Node {
        value_t value;
        Config config;
        int priority;
        Node *lchild = nullptr, *rchild = nullptr;

        explicit Node(value_t value) : value(value), config(value), priority(std::rand()) {}

        size_t index() { return get_config(lchild).size; }

        Node *set_children(Node *lchild, Node *rchild) {
            this->lchild = lchild;
            this->rchild = rchild;
            update();
            return this;
        }

        void update() {
            config = Config(value);
            config = ConfigM::MAPPEND(get_config(lchild), config);
            config = ConfigM::MAPPEND(config, get_config(rchild));
        }
    };

    static Config
    get_config(Node *node) { return node ? node->config : ConfigM::MEMPTY(); }

    static Node *
    merge(Node *left, Node *right) {
        if (left == nullptr) { return right; }
        if (right == nullptr) { return left; }
        if (left->priority < right->priority) {
            auto right_part = merge(left->rchild, right);
            return left->set_children(left->lchild, right_part);
        } else {
            auto left_part = merge(left, right->lchild);
            return right->set_children(left_part, right->rchild);
        }
    }

    static Node *
    merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

    static std::pair<Node *, Node *>
    split(Node *root, size_t index /* left part size */) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (index <= root->index()) {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->lchild, index);
            root->set_children(right_part, root->rchild);
            return {left_part, root};
        } else {
            Node *left_part, *right_part;
            size_t new_index = index < (root->index() + root->value.size)
                               ? 0 : (index - root->index() - root->value.size);
            std::tie(left_part, right_part) = split(root->rchild, new_index);
            root->set_children(root->lchild, left_part);
            return {root, right_part};
        }
    };

    static std::tuple<Node *, Node *, Node *>
    split(Node *root, size_t lindex, size_t rindex) {
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, rindex);
        std::tie(left, middle) = split(left, lindex);
        return {left, middle, right};
    };

    static void print(Node *root, std::ostream &out, std::string const &indent) {
        if (root) {
            print(root->rchild, out, indent + "\t");
            out << indent << char(root->value.code + 'a') << ":" << root->value.size << "\n";
            print(root->lchild, out, indent + "\t");
        }
    }

    static void fill(Node *root, std::vector<int> &array) {
        if (root) {
            fill(root->lchild, array);
            array.emplace_back(root->value.code);
            fill(root->rchild, array);
        }
    }

    static std::pair<Node *, Node *>
    separate_rightmost(Node *root) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (root->rchild == nullptr) {
            Node *left_part = root->lchild;
            root->set_children(nullptr, nullptr);
            return {left_part, root};
        }
        Node *left_part, *right_part;
        std::tie(left_part, right_part) = separate_rightmost(root->rchild);
        root->set_children(root->lchild, left_part);
        return {root, right_part};
    };


private:
    Node *root = nullptr;

public:
    Treap() = default;

    size_t size() { return get_config(root).size; }

    void insert(value_t x, size_t index) {
        if (index == 0) {
            root = merge(new Node(x), root);
            return;
        }
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, index + 1);
        size_t diff = left->config.size - index;
        std::tie(left, middle) = separate_rightmost(left);
        Node *before, *after;
        before = new Node({middle->value.code, middle->value.size - diff});
        after = new Node({middle->value.code, diff});
        if (before->value.size == 0) { before = nullptr; }
        if (after->value.size == 0) { after = nullptr; }
        root = merge(left, merge(before, new Node(x), after), right);
    }

    void remove(size_t index, size_t number) {
        Node *left, *middle, *right, *excluded;
        std::tie(left, right) = split(root, index + number + 1);
        size_t diff1 = left->config.size - index - number;
        std::tie(left, middle) = separate_rightmost(left);
        Node *after1;
        after1 = new Node({middle->value.code, diff1});
        if (after1->value.size == 0) { after1 = nullptr; }

        left = merge(left, middle);
        std::tie(left, excluded) = split(left, index + 1);
        size_t diff2 = left->config.size - index;
        std::tie(left, middle) = separate_rightmost(left);
        Node *before2;
        before2 = new Node({middle->value.code, middle->value.size - diff2});
        if (before2->value.size == 0) { before2 = nullptr; }

        if (before2 and after1 and before2->value.code == after1->value.code) {
            root = merge(left, new Node({before2->value.code, before2->value.size + after1->value.size}), right);
        } else {
            root = merge(left, merge(before2, after1), right);
        }
    }

    void print(std::ostream &out) { print(root, out, ""); }

    std::vector<int> list() {
        std::vector<int> array;
        array.reserve(size());
        fill(root, array);
        return array;
    }

    int bulk(size_t l, size_t r) {
        Node *left, *middle, *right, *temp;
        std::tie(left, middle, right) = split(root, l + 1, r + 1);
        std::tie(left, temp) = separate_rightmost(left);
        middle = merge(temp, middle);
        auto result = __builtin_popcount(get_config(middle).alphabet);
        root = merge(left, middle, right);
        return result;
    }
};

void solve(std::istream &in, std::ostream &out) {
    Treap treap;
    size_t n;
    in >> n;
    while (n--) {
        std::string instruction;
        in >> instruction;
        if (instruction == "+") {
            std::size_t index, number;
            char letter;
            in >> index >> number >> letter;
            value_t value = {size_t(letter - 'a'), number};
            treap.insert(value, index - 1);
        } else if (instruction == "-") {
            size_t index, number;
            in >> index >> number;
            treap.remove(index - 1, number);
        } else {
            size_t l, r;
            in >> l >> r;
            out << treap.bulk(l - 1, r - 1) << "\n";
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin("log.in");
        std::ofstream fout("log.out");
        solve(fin, fout);
    }
    return 0;
}