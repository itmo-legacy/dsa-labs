#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <experimental/optional>

using std::size_t;
using std::experimental::optional;

using value_t = int;

class Treap {
    struct Node {
        value_t value;
        int priority;
        size_t size = 1;
        Node *lchild = nullptr, *rchild = nullptr;

        explicit Node(value_t value) : value(value), priority(std::rand()) {}

        size_t key() { return get_size(lchild); }

        Node *set_children(Node *lchild, Node *rchild) {
            this->lchild = lchild;
            this->rchild = rchild;
            update();
            return this;
        }

        void update() {
            size = get_size(lchild) + get_size(rchild) + 1;
        }
    };

    static size_t get_size(Node *node) { return node ? node->size : 0; }

    static Node *
    merge(Node *left, Node *right) {
        if (left == nullptr) { return right; }
        if (right == nullptr) { return left; }
        if (left->priority > right->priority) {
            auto right_part = merge(left->rchild, right);
            return left->set_children(left->lchild, right_part);
        } else {
            auto left_part = merge(left, right->lchild);
            return right->set_children(left_part, right->rchild);
        }
    }

    static Node *
    merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

    static std::pair<Node *, Node *>
    split(Node *root, size_t key /* left part size */) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (key <= root->key()) {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->lchild, key);
            root->set_children(right_part, root->rchild);
            return {left_part, root};
        } else {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->rchild, key - root->key() - 1);
            root->set_children(root->lchild, left_part);
            return {root, right_part};
        }
    };

    static size_t
    find_key(Node *root, value_t value) {
        if (root == nullptr) { return 0; }
        if (value <= root->value) { return find_key(root->lchild, value); }
        else { return find_key(root->rchild, value) + root->key() + 1; }
    }

    size_t find_key(value_t value) { return find_key(root, value); }

    Node *find_node(value_t value) { return find_kth(find_key(value)); }

    Node *find_kth(size_t k) {
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, k + 1);
        std::tie(left, middle) = split(left, k);
        auto result = middle;
        root = merge(left, middle, right);
        return result;
    }

private:
    Node *root = nullptr;

public:
    void insert(value_t x) {
        if (not contains(x)) {
            size_t key = find_key(x);
            Node *left, *right;
            std::tie(left, right) = split(root, key);
            root = merge(left, new Node(x), right);
        }
    }

    void remove(value_t x) {
        if (contains(x)) {
            size_t key = find_key(x);
            Node *left, *middle, *right;
            std::tie(left, right) = split(root, key + 1);
            std::tie(left, middle) = split(left, key);
            root = merge(left, right);
        }
    }

    bool contains(value_t x) {
        auto node = find_node(x);
        return node != nullptr and node->value == x;
    }

    optional<value_t> next(value_t x) {
        auto node = find_node(x + 1);
        if (node) { return {node->value}; }
        else { return {}; }
    }

    optional<value_t> prev(value_t x) {
        auto node = find_kth(find_key(x) - 1);
        if (node) { return {node->value}; }
        else { return {}; }
    }

    void print(Node *root, std::ostream &out) {
        if (root) {
            print(root->lchild, out);
            out << " " << root->value << " ";
            print(root->rchild, out);
        }
    }

    void print(std::ostream &out) {
        print(root, out);
        out << std::endl;
    }
};

void solve(std::istream &in, std::ostream &out) {
    std::string instruction;
    Treap treap;
    out << std::boolalpha;
    while (in >> instruction) {
        value_t x;
        in >> x;
        if (instruction == "insert") {
            treap.insert(x);
        } else if (instruction == "delete") {
            treap.remove(x);
        } else if (instruction == "exists") {
            out << treap.contains(x) << "\n";
        } else if (instruction == "next") {
            auto maybe_next = treap.next(x);
            if (maybe_next) { out << maybe_next.value() << "\n"; }
            else { out << "none\n"; }
        } else if (instruction == "prev") {
            auto maybe_prev = treap.prev(x);
            if (maybe_prev) { out << maybe_prev.value() << "\n"; }
            else { out << "none\n"; }
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
//        std::ifstream fin(taskname + ".in");
//        std::ofstream fout(taskname + ".out");
//        solve(fin, fout);
        solve(std::cin, std::cout);
    }
    return 0;
}