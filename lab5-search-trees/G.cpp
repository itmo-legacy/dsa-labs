#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <experimental/optional>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>
#include <unordered_set>
#include <cstdlib>

using std::size_t;
using std::experimental::optional;

constexpr int CAP = 1'000'000'000;

using value_t = int;

template<typename T>
struct Monoid {
    static const T MEMPTY() { return T(); };

    static T MAPPEND(T const &one, T const &two) { return one + two; }
};

class Treap {
    struct Config {
        size_t size = 0;
        long long sum = 0;

        Config() = default;

        explicit Config(value_t value) : size(1), sum(value) {};

        Config(size_t size, long long sum) : size(size), sum(sum) {};

        bool operator==(Config const &rhs) const {
            return size == rhs.size and sum == rhs.sum;
        };
    };

    struct ConfigM : public Monoid<Config> {
        static Config MAPPEND(Config const &one, Config const &two) {
            return {one.size + two.size,
                    one.sum + two.sum};
        }
    };

    struct Node {
        value_t value;
        Config config;
        int priority;
        Node *lchild = nullptr, *rchild = nullptr;

        explicit Node(value_t value) : value(value), config(value), priority(std::rand()) {}

        size_t index() { return get_config(lchild).size; }

        Node *set_children(Node *lchild, Node *rchild) {
            this->lchild = lchild;
            this->rchild = rchild;
            update();
            return this;
        }

        void update() {
            config = Config(value);
            config = ConfigM::MAPPEND(get_config(lchild), config);
            config = ConfigM::MAPPEND(config, get_config(rchild));
        }
    };

    static Config
    get_config(Node *node) { return node ? node->config : ConfigM::MEMPTY(); }

    static Node *
    merge(Node *left, Node *right) {
        if (left == nullptr) { return right; }
        if (right == nullptr) { return left; }
        if (left->priority < right->priority) {
            auto right_part = merge(left->rchild, right);
            return left->set_children(left->lchild, right_part);
        } else {
            auto left_part = merge(left, right->lchild);
            return right->set_children(left_part, right->rchild);
        }
    }

    static Node *
    merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

    static std::pair<Node *, Node *>
    split(Node *root, size_t index /* left part size */) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (index <= root->index()) {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->lchild, index);
            root->set_children(right_part, root->rchild);
            return {left_part, root};
        } else {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->rchild, index - root->index() - 1);
            root->set_children(root->lchild, left_part);
            return {root, right_part};
        }
    };

    static std::tuple<Node *, Node *, Node *>
    split(Node *root, size_t lindex, size_t rindex) {
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, rindex);
        std::tie(left, middle) = split(left, lindex);
        return {left, middle, right};
    };

    void print(Node *root, std::ostream &out, std::string const &indent) {
        if (root) {
            print(root->rchild, out, indent + "\t");
            out << indent << root->value << "\n";
            print(root->lchild, out, indent + "\t");
        }
    }

    void fill(Node *root, std::vector<int> &array) {
        if (root) {
            fill(root->lchild, array);
            array.emplace_back(root->value);
            fill(root->rchild, array);
        }
    }

private:
    Node *root = nullptr;

public:
    Treap() = default;

    size_t size() { return get_config(root).size; }

    void insert(value_t x) {
        root = merge(root, new Node(x));
    }

    void print(std::ostream &out) { print(root, out, ""); }

    std::vector<int> list() {
        std::vector<int> array;
        array.reserve(size());
        fill(root, array);
        return array;
    }

    void rearrange(size_t l, size_t r) {
        Node *left, *middle, *right;
        std::tie(left, middle, right) = split(root, l, r);
        root = merge(middle, left, right);
    }
};

void solve(std::istream &in, std::ostream &out) {
    Treap treap;
    size_t n, m;
    in >> n >> m;
    for (std::size_t i = 0; i < n; ++i) {
        treap.insert(int(i + 1));
    }
    while (m--) {
        size_t l, r;
        in >> l >> r;
        treap.rearrange(l - 1, r);
    }
    std::vector<value_t> list = treap.list();
    for (auto &&item: list) {
        out << item << " ";
    }
    out << std::endl;
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        solve(std::cin, std::cout);
    }
    return 0;
}