#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <experimental/optional>
#include <vector>
#include <algorithm>
#include <stack>
#include <queue>

using std::size_t;
using std::experimental::optional;

using value_t = int;

struct Vertex {
    value_t value;
    int priority;
    size_t index;

    bool operator<(Vertex const &rhs) const {
        return value > rhs.value;
    }
};

struct Family {
    size_t parent = size_t(-1);
    size_t lchild = size_t(-1);
    size_t rchild = size_t(-1);
};

class Treap {
    struct Node {
        value_t value;
        size_t index;
        int priority;
        size_t size = 1;
        Node *lchild = nullptr, *rchild = nullptr;

        explicit Node(value_t value) : value(value), priority(std::rand()) {}

        Node(value_t value, size_t index, int priority) : value(value), index(index), priority(priority) {}


        size_t key() { return get_size(lchild); }

        Node *set_children(Node *lchild, Node *rchild) {
            this->lchild = lchild;
            this->rchild = rchild;
            update();
            return this;
        }

        void update() {
            size = get_size(lchild) + get_size(rchild) + 1;
        }
    };

    static size_t get_size(Node *node) { return node ? node->size : 0; }

    static size_t get_index(Node *node) { return node ? node->index : size_t(-1); }

    static Node *
    merge(Node *left, Node *right) {
        if (left == nullptr) { return right; }
        if (right == nullptr) { return left; }
        if (left->priority < right->priority) {
            auto right_part = merge(left->rchild, right);
            return left->set_children(left->lchild, right_part);
        } else {
            auto left_part = merge(left, right->lchild);
            return right->set_children(left_part, right->rchild);
        }
    }

    static Node *
    merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

    static std::pair<Node *, Node *>
    split(Node *root, size_t key /* left part size */) {
        if (root == nullptr) { return {nullptr, nullptr}; }
        if (key <= root->key()) {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->lchild, key);
            root->set_children(right_part, root->rchild);
            return {left_part, root};
        } else {
            Node *left_part, *right_part;
            std::tie(left_part, right_part) = split(root->rchild, key - root->key() - 1);
            root->set_children(root->lchild, left_part);
            return {root, right_part};
        }
    };

    static size_t
    find_key(Node *root, value_t value) {
        if (root == nullptr) { return 0; }
        if (value <= root->value) { return find_key(root->lchild, value); }
        else { return find_key(root->rchild, value) + root->key() + 1; }
    }

    size_t find_key(value_t value) { return find_key(root, value); }

    Node *find_node(value_t value) { return find_kth(find_key(value)); }

    Node *find_kth(size_t k) {
        Node *left, *middle, *right;
        std::tie(left, right) = split(root, k + 1);
        std::tie(left, middle) = split(left, k);
        auto result = middle;
        root = merge(left, middle, right);
        return result;
    }

    void print(Node *root, std::ostream &out, std::string const &indent) {
        if (root) {
            print(root->rchild, out, indent + "\t");
            out << indent << root->value << ":" << root->priority << "\n";
            print(root->lchild, out, indent + "\t");
        }
    }

    void fill(Node *node, size_t parentIndex, std::vector<Family> &families) {
        if (node) {
            families[node->index] = {parentIndex, get_index(node->lchild), get_index(node->rchild)};
            fill(node->lchild, node->index, families);
            fill(node->rchild, node->index, families);
        }
    }

private:

    Node *root = nullptr;
public:
    size_t size() {
        return get_size(root);
    }

    void merge(Treap const &another) {
        this->root = merge(root, another.root);
    }

    void insert(value_t x) {
        if (not contains(x)) {
            size_t key = find_key(x);
            Node *left, *right;
            std::tie(left, right) = split(root, key);
            root = merge(left, new Node(x), right);
        }
    }

    void insert(Vertex const &v) {
        if (not contains(v.value)) {
            size_t key = find_key(v.value);
            Node *left, *right;
            std::tie(left, right) = split(root, key);
            root = merge(left, new Node(v.value, v.index, v.priority), right);
        }
    }

    void remove(value_t x) {
        if (contains(x)) {
            size_t key = find_key(x);
            Node *left, *middle, *right;
            std::tie(left, right) = split(root, key + 1);
            std::tie(left, middle) = split(left, key);
            root = merge(left, right);
        }
    }

    bool contains(value_t x) {
        auto node = find_node(x);
        return node != nullptr and node->value == x;
    }

    optional<value_t> next(value_t x) {
        auto node = find_node(x + 1);
        if (node) { return {node->value}; }
        else { return {}; }
    }

    optional<value_t> prev(value_t x) {
        auto node = find_kth(find_key(x) - 1);
        if (node) { return {node->value}; }
        else { return {}; }
    }

    void print(std::ostream &out) {
        print(root, out, "");
    }

    void fill(std::vector<Family> &families) {
        fill(root, size_t(-1), families);
    };
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::deque<Vertex> vertices(n);
    for (size_t i = 0; i < n; ++i) {
        in >> vertices[i].value >> vertices[i].priority;
        vertices[i].index = i;
    }
    std::sort(vertices.begin(), vertices.end());
    std::queue<Treap> queue;
    for (auto &&vertex: vertices) {
        queue.emplace();
        queue.back().insert(vertex);
    }
    while (queue.size() != 1) {
        std::queue<Treap> next_level;
        while (queue.size() > 1) {
            auto &top1 = queue.front();
            queue.pop();
            auto &top2 = queue.front();
            queue.pop();
            top2.merge(top1);
            next_level.push(top2);
        }
        if (not queue.empty()) { next_level.push(queue.back()); }
        queue = next_level;
    }
    Treap &treap = queue.front();
    std::vector<Family> families(n);
    treap.fill(families);
    out << "YES\n";
    for (auto &&item: families) {
        out << item.parent + 1 << " " << item.lchild + 1 << " " << item.rchild + 1 << "\n";
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        solve(fin, std::cout);
    } else {
        solve(std::cin, std::cout);
    }
    return 0;
}