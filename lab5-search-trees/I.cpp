#include <fstream>
#include <cstring>
#include <iostream>
#include <tuple>
#include <vector>

using std::size_t;

struct Node;

using value_t = int;

struct Node {
    value_t value;
    int priority;
    Node *parent;
    bool is_left;
    Node *lchild = nullptr, *rchild = nullptr;
    size_t size = 1;
    bool reverse = false;
    bool is_cycle = false;

    explicit Node(value_t value) : value(value), priority(std::rand()), parent(this), is_left(true) {}

    size_t index() {
        push_reverse();
        return _index();
    }

    size_t _index() const { return get_size(lchild); }

    size_t number() {
        push_reverse();
        return _number();
    }

    size_t _number() {
        if (this == parent) { return index(); }
        if (is_left) {
            return parent->number() - 1 - get_size(rchild);
        } else {
            return parent->number() + 1 + index();
        }
    }

    void push_reverse() {
        if (this != parent) { parent->push_reverse(); }
        update();
    }

    Node *get_root() {
        if (this == parent) { return this; }
        else { return parent->get_root(); }
    }

    Node *set_children(Node *lchild, Node *rchild) {
        this->lchild = lchild;
        this->rchild = rchild;
        update();
        return this;
    }

    void update() {
        size = 1;
        if (reverse) {
            std::swap(lchild, rchild);
            if (lchild) {
                lchild->is_left = true;
                lchild->reverse ^= 1;
            }
            if (rchild) {
                rchild->is_left = false;
                rchild->reverse ^= 1;
            }
            reverse = false;
        }

        if (lchild) {
            size += lchild->size;
            lchild->parent = this;
            lchild->is_left = true;
//            lchild->is_cycle = is_cycle;
        }
        if (rchild) {
            size += rchild->size;
            rchild->parent = this;
            rchild->is_left = false;
//            rchild->is_cycle = is_cycle;
        }
    }

    friend size_t get_size(Node *node) { return node ? node->size : 0; }
};

Node *
merge(Node *left, Node *right) {
    if (left == nullptr) { return right; }
    if (right == nullptr) { return left; }
    left->update();
    right->update();
    if (left->priority < right->priority) {
        auto right_part = merge(left->rchild, right);
        return left->set_children(left->lchild, right_part);
    } else {
        auto left_part = merge(left, right->lchild);
        return right->set_children(left_part, right->rchild);
    }
}

static Node *
merge(Node *n1, Node *n2, Node *n3) { return merge(merge(n1, n2), n3); }

static std::pair<Node *, Node *>
split(Node *root, size_t index /* left part size */) {
    if (root == nullptr) { return {nullptr, nullptr}; }
    root->update();
    if (index <= root->index()) {
        Node *left_part, *right_part;
        std::tie(left_part, right_part) = split(root->lchild, index);
        if (left_part) { left_part->parent = left_part; }
        root->set_children(right_part, root->rchild);
        return {left_part, root};
    } else {
        Node *left_part, *right_part;
        std::tie(left_part, right_part) = split(root->rchild, index - root->index() - 1);
        if (right_part) { right_part->parent = right_part; }
        root->set_children(root->lchild, left_part);
        return {root, right_part};
    }
};

static std::tuple<Node *, Node *, Node *>
split(Node *root, size_t lindex, size_t rindex) {
    Node *left, *middle, *right;
    std::tie(left, right) = split(root, rindex);
    std::tie(left, middle) = split(left, lindex);
    return std::make_tuple(left, middle, right);
};

static void print(Node *root, std::ostream &out, std::string const &indent) {
    if (root) {
        print(root->rchild, out, indent + "\t");
        out << indent << root->value + 1 << (root->is_cycle ? ":c" : "") << (root->reverse ? ":r" : "") << "\n";
        print(root->lchild, out, indent + "\t");
    } else {
//        out << indent << "*" << "\n";
    }
}

void connect(Node &one, Node &two) {
    Node *parent1 = one.get_root();
    Node *parent2 = two.get_root();
    if (parent1 == parent2) {
        parent1->is_cycle = true;
#ifdef DEBUG
        print(parent1, std::cout, ">|");
        std::cout << std::endl;
#endif
    } else {
        one.push_reverse();
        two.push_reverse();
        if (one.number() == 0) { parent1->reverse ^= 1; }
        if (two.number() != 0) { parent2->reverse ^= 1; }
        const auto root = merge(parent1, parent2);
#ifdef DEBUG
        print(root, std::cout, ">|");
        std::cout << std::endl;
#endif
    }
}

void disconnect(Node &one, Node &two) {
    Node *parent = one.get_root();
    size_t number1 = one.number(), number2 = two.number();
    size_t split_number = std::max(number1, number2);
    if (split_number - std::min(number1, number2) > 1) {
        split_number = 0;
    }
    Node *l, *r;
    std::tie(l, r) = split(parent, split_number);
    if (parent->is_cycle) {
        merge(r, l)->is_cycle = false;
#ifdef DEBUG
        print(parent->get_root(), std::cout, ">|");
        std::cout << std::endl;
    } else {
        print(l, std::cout, ">|");
        std::cout << "-" << std::endl;
        print(r, std::cout, ">|");
        std::cout << std::endl;
#endif
    }
}

int distance(Node &one, Node &two) {
    Node *parent1 = one.get_root();
    Node *parent2 = two.get_root();
    if (parent1 != parent2) {
        return -1;
    } else {
#ifdef DEBUG
        print(parent1, std::cout, ">>|");
        std::cout << "\n";
#endif
        bool is_cycle = parent1->is_cycle;
        size_t number1 = one.number();
        size_t number2 = two.number();
        size_t result = std::max(number1, number2) - std::min(number1, number2);
        if (is_cycle) {
            result = std::min(result, parent1->size - result);
        }
#ifdef DEBUG
        print(parent1->get_root(), std::cout, ">|");
        std::cout << std::endl;
#endif
        return std::max(int(result) - 1, 0);
    }
}

void solve(std::istream &in, std::ostream &out) {
    size_t n, m, q;
    in >> n >> m >> q;
    std::vector<Node> nodes;
    nodes.reserve(n);
    for (std::size_t i = 0; i < n; ++i) { nodes.emplace_back(int(i)); }
    for (std::size_t _ = 0; _ < m; ++_) {
        size_t i, j;
        in >> i >> j;
        connect(nodes[i - 1], nodes[j - 1]);
    }
    for (std::size_t _ = 0; _ < q; ++_) {
        std::string instruction;
        size_t i, j;
        in >> instruction >> i >> j;
        if (instruction == "+") {
            connect(nodes[i - 1], nodes[j - 1]);
        } else if (instruction == "-") {
            disconnect(nodes[i - 1], nodes[j - 1]);
        } else {
            if (i == j) {
                out << "0\n";
            } else {
                out << distance(nodes[i - 1], nodes[j - 1]) << "\n";
            }
        }
    }
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
        std::ofstream fout("output.txt");
        solve(fin, fout);
//        solve(fin, std::cout);
    } else {
        std::ifstream fin("roads.in");
        std::ofstream fout("roads.out");
        solve(fin, fout);
    }
    return 0;
}