#include <cstring>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using std::size_t;

std::string const taskname = "request";

struct Event {
    unsigned start;
    unsigned end;
};

void solve(std::istream &in, std::ostream &out) {
    size_t n;
    in >> n;
    std::vector<Event> events(n);
    for (size_t i = 0; i < n; ++i) {
        unsigned s, f;
        in >> s >> f;
        events[i] = {s, f};
    }
    std::sort(events.begin(), events.end(), [](const Event &a, const Event &b) { return a.end < b.end; });
    unsigned result = 0;
    unsigned last_end = 0;
    for (auto &&event: events) {
        if (event.start >= last_end) {
            ++result;
            last_end = event.end;
        }
    }
    out << result << "\n";
}

int main(int argc, char *argv[]) {
    bool is_local = argc >= 2 && strcmp(argv[1], "-l") == 0;
    if (is_local) {
        std::ifstream fin("input.txt");
//        std::ofstream fout("output.txt");
        solve(fin, std::cout);
    } else {
        std::ifstream fin(taskname + ".in");
        std::ofstream fout(taskname + ".out");
        solve(fin, fout);
//        std::cin.tie(nullptr);
//        std::ios::sync_with_stdio(false);
//        solve(std::cin, std::cout);
    }
}
